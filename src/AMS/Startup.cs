﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMS.Data;
using AMS.Data.Repos;
using AMS.Data.Repos.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Http;

namespace AMS
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region Dependency Injection

            services.AddTransient<ISaleRepository, SaleRepository>();
            services.AddTransient<IModuleRepository, ModuleRepository>();
            services.AddTransient<ISystemUserRepository, SystemUserRepository>();
            services.AddTransient<ISaleFileRepository, SaleFileRepository>();
            services.AddTransient<ISaleFinalTemplateRepository, SaleFinalTemplateRepository>();
            services.AddTransient<IEvaluationRepository, EvaluationRepository>();
            services.AddTransient<IEvaluationFilesRepository, EvaluationFilesRepository>();
            services.AddTransient<IEvaluationFinalTemplateRepository, EvaluationFinalTemplateRepository>();
            services.AddTransient<ISaleTemplateFileRepository, SaleTemplateFileRepository>();
            services.AddTransient<IEvaluationTemplateFileRepository, EvaluationTemplateFileRepository>();
            services.AddTransient<IDailyMovmentRepository, DailyMovmentRepository>();
            services.AddTransient<ILotRepository, LotRepository>();
            services.AddTransient<ILot_BuyersRepository, Lot_BuyersRepository>();
            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<ICustomerRepository, CustomerRepository>();
            services.AddTransient<IMixedCalculationRepository, MixedCalculationRepository>();
            services.AddTransient<IMixedCalculation_LotsRepository, MixedCalculation_LotsRepository>();
            services.AddTransient<IMixedCalculation_transactionsRepository, MixedCalculation_transactionsRepository>();

            services.AddScoped<AmsDbContext>();


            #endregion
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(10);//You can set Time   
            });
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseSession();
            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=SignIn}");

                routes.MapRoute(
name: "signin",
template: "signin",
defaults: new { controller = "Home", action = "SignIn" });

                routes.MapRoute(
    name: "MainMenu",
    template: "homepage",
    defaults: new { controller = "Home", action = "HomePage" });
                #region Sale Routes

                routes.MapRoute(
        name: "sale",
        template: "Sales/",
        defaults: new { controller = "Sales", action = "Index" });


                routes.MapRoute(
           name: "New_sale",
           template: "Sales/add/step_1",
           defaults: new { controller = "Sales", action = "add_step1" });

                routes.MapRoute(
     name: "edit_sale",
     template: "Sales/{salecode}/edit",
     defaults: new { controller = "Sales", action = "EditSale" },
                    constraints: new { salecode = "^[a-zA-Z0-9]{10}$" });

                routes.MapRoute(
  name: "view_sale",
  template: "sales/{salecode}/",
  defaults: new { controller = "Sales", action = "ViewSale" },
                 constraints: new { salecode = "^[a-zA-Z0-9]{10}$" });


                #endregion

                #region EvaluationsRoute
                routes.MapRoute(
             name: "evaluation",
             template: "evaluations/",
             defaults: new { controller = "Evaluation", action = "Index" });

                routes.MapRoute(
name: "newevaluation",
template: "evaluations/add/step_1",
defaults: new { controller = "Evaluation", action = "add_step1" });

                routes.MapRoute(
name: "edit_evaluation",
template: "evaluations/{evaluationcode}/edit",
defaults: new { controller = "Evaluation", action = "EditEvaluation" },
           constraints: new { evaluationcode = "^[a-zA-Z0-9]{10}$" });
                routes.MapRoute(
name: "view_evaluation",
template: "evaluations/{evaluationcode}/",
defaults: new { controller = "Evaluation", action = "ViewEvaluation" },
constraints: new { evaluationcode = "^[a-zA-Z0-9]{10}$" });
                #endregion

                #region DailyMovmentRoutes
                routes.MapRoute(
           name: "DailyMovments",
           template: "movments/",
           defaults: new { controller = "DailyMovment", action = "Index" });
                #endregion


                #region CalculationsRoutes
                routes.MapRoute(
      name: "calculation",
      template: "calculations/",
      defaults: new { controller = "Calculations", action = "Index" });

                routes.MapRoute(
name: "calculationadd",
template: "calculations/add",
defaults: new { controller = "Calculations", action = "Add_Lot" });

                routes.MapRoute(
name: "Calculationsaleddetails",
template: "calculations/sale/details/{saleId}",
defaults: new { controller = "Calculations", action = "Sale_LotDetails" },
constraints: new { saleId = "^[1-9][0-9]*$" });

                routes.MapRoute(
name: "Calculationmixed",
template: "calculations/Mixed/{saleId}",
defaults: new { controller = "Calculations", action = "MixedCalculations" },
constraints: new { saleId = "^[1-9][0-9]*$" });

                routes.MapRoute(
name: "CalculationmixedNew",
template: "calculations/Mixed/{saleId}/add",
defaults: new { controller = "Calculations", action = "MixedCalculation_Add" },
constraints: new { saleId = "^[1-9][0-9]*$" });
                routes.MapRoute(
name: "Calculationmixeddetails",
template: "calculations/Mixed/{saleId}/details/{mixedcalcId}",
defaults: new { controller = "Calculations", action = "MixedCalculation_details" },
constraints: new { saleId = "^[1-9][0-9]*$" , mixedcalcId = "^[1-9][0-9]*$" });
                #endregion
            });




        }
    }
}
