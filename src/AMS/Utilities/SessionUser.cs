﻿using AMS.ViewModels.user;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.Utilities
{
    public static class SessionUser
    {
        public static void SetUserDataSession(IHttpContextAccessor httpContextAccessor, UserInfo user)
        {


            if (user != null && user.UserId != 0 && user.UserName != null && user.UserName != "")
            {
                user.IsSignedIn = true;

            }
            httpContextAccessor.HttpContext.Session.Set<UserInfo>("SessionUser", user);
            httpContextAccessor.HttpContext.Session.SetString("userId", user.UserId.ToString());
            httpContextAccessor.HttpContext.Session.Set<string>("userName", user.UserName);
            httpContextAccessor.HttpContext.Session.Set<int>("userRole", user.UserRole);

        }

        public static UserInfo GetUserDataSession(IHttpContextAccessor httpContextAccessor)
        {
            UserInfo userData = new UserInfo();
            userData = httpContextAccessor.HttpContext.Session.Get<UserInfo>("SessionUser");

            return userData;
        }

    }
}
