﻿using AMS.Data.Repos.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.Utilities
{
    public static class RandomCode
    {
        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }


    public class GenerateCode
    {
        private ISaleRepository _saleRepository { get; set; }
        private IEvaluationRepository _evaluationRepository { get; set; }

        public GenerateCode(ISaleRepository saleRepository,IEvaluationRepository evaluationRepository)
        {
            _saleRepository = saleRepository;
            _evaluationRepository = evaluationRepository;
        }

        public string GenerateValidCode()
        {
            bool valid = true;
            string code = "";
            do
            {

                code = RandomCode.RandomString(10);

                var check = _saleRepository.Filter(x => x.Code == code).FirstOrDefault();
                if (check != null)
                    valid = false;

            } while (!valid);
            return code;
        }

        public string GenerateValidEvaluationCode()
        {
            bool valid = true;
            string code = "";
            do
            {

                code = RandomCode.RandomString(10);

                var check = _evaluationRepository.Filter(x => x.code == code).FirstOrDefault();
                if (check != null)
                    valid = false;

            } while (!valid);
            return code;
        }
    }


}
