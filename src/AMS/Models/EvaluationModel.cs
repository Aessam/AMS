﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.Models
{
    public class EvaluationModel
    {

        public string code { get; set; }

        public string ClientName { get; set; }

        public int EvaluationType { get; set; }

        public DateTime? PreviewDate { get; set; }
        public DateTime? PreprationDate { get; set; }
        public decimal? Fees { get; set; }
        public int? CollectionStatus { get; set; }

        public int? CollectionType { get; set; }

        public decimal? ValueAddedTax { get; set; }
        public int? SupplyStatus { get; set; }
        public DateTime? Supplydate { get; set; }

        public int? generalTaxStatus { get; set; }

        public decimal? generaltax { get; set; }
        public int EvaluationStatus { get; set; }

    }
}
