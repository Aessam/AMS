﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.Models
{
    public class SaleModel
    {
        
       
        public string InstitutionName { get; set; }
        public DateTime? Date { get; set; }
        public string Description { get; set; }
        public decimal? TotalSales { get; set; }
        public decimal? CommissionRate { get; set; }
        public decimal? Commission { get; set; }
        public int? CollectionStatus { get; set; }
        public int? CollectionType { get; set; }
        public decimal? Fees { get; set; }
        public int? SupplyStatus { get; set; }
        public decimal? ValueAddedTax { get; set; }
        public DateTime? SupplyDate { get; set; }

        public int SaleStatus { get; set; }

        public int ValueAddedStatus { get; set; }
        public DateTime? ValueAddedDate { get; set; }

        public int? SaleType { get; set; }
        public string Code { get; set; }
    }
}
