﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.ViewModels.DailyMovment
{
    public class dailymovmentVM
    {
        public DateTime movmentdate { get; set; }

        public TimeSpan movmenttime { get; set; }

        public string movmentdetails { get; set; }
    }
}
