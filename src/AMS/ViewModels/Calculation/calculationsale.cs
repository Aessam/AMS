﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.ViewModels.Calculation
{
    public class calculationsale
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string SaleName { get; set; }
        public DateTime date { get; set; }
    }
}
