﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.ViewModels.Calculation
{
    public class Sale_LotdetailsVM
    {
        public int LotId { get; set; }

        public string LotNumber { get; set; }

        public string BuyersName { get; set; }

        public string LotType { get; set; }
        public decimal BuyValue { get; set; }
        public decimal PaidAmount { get; set; }
        public decimal BuyValueWithFees { get; set; }
        public decimal InsuranceValue { get; set; }
        public decimal Total { get; set; }
        public decimal Reminder { get; set; }
        public decimal InsurancePercentage { get; set; }
        public decimal FeesPercentage { get; set; }
        public List<buyesrObject> buyers { get; set; }
    }

    public class buyesrObject
    { public int CustomerId { get; set; } }
}
