﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.ViewModels.Calculation
{
    public class AddLotVM
    {
        public string LotNumber { get; set; }
        public int saleID { get; set; }
        public string LotType { get; set; }
        public decimal BuyValue { get; set; }
        public int Weightornumber { get; set; }
        public decimal priceperweightornumber { get; set; }
        public decimal paidamount { get; set; }
        public decimal InsurancePercentage { get; set; }
        public decimal FeesPercentage { get; set; }
        public decimal Total { get; set; }
        public decimal Reminder { get; set; }
        public List<buyersObject> customers { get; set; }
    }
    public class buyersObject
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
