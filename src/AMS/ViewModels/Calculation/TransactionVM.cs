﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.ViewModels.Calculation
{
    public class TransactionVM
    {
        public int MixedCalculationId { get; set; }
        public decimal Paidamount { get; set; }
        public string PaidDescription { get; set; }

        public string type { get; set; }
    }
}
