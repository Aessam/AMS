﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.ViewModels.Evaluation
{
    public class EvaluationVM
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public string ClientName { get; set; }

        public DateTime? Date { get; set; }
        public int status { get; set; }

        public string statusstring { get; set; }
    }
}
