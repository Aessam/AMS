﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.ViewModels.Evaluation
{
    public class EvaluationInfoVM
    {
        public int Id { get; set; }
       
        public string code { get; set; }

        public string ClientName { get; set; }

        public int EvaluationType { get; set; }

        public DateTime? PreviewDate { get; set; }
        public DateTime? PreprationDate { get; set; }
        public decimal? Fees { get; set; }
        public int? CollectionStatus { get; set; }

        public int? CollectionType { get; set; }

        public decimal? ValueAddedTax { get; set; }
        public int? SupplyStatus { get; set; }
        public DateTime? Supplydate { get; set; }

        public int? generalTaxStatus { get; set; }

        public decimal? generaltax { get; set; }
        public int EvaluationStatus { get; set; }
        public List<EvaluationFilesVM> files { get; set; }

        public List<EvaluationTemplatesVM> finalTemplates { get; set; }
        public List<TemplatesVM> Templates { get; set; }
    }

    public class EvaluationFilesVM
    {
        public int Id { get; set; }
        public string filename { get; set; }
        public string description { get; set; }
        public DateTime uploaddate { get; set; }
        public string FilePath { get; set; }
    }

    public class EvaluationTemplatesVM
    {
        public int Id { get; set; }
        public string filePath { get; set; }

        public DateTime uploaddate { get; set; }
    }
}
