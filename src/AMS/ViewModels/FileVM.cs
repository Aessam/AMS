﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.ViewModels
{
    public class FileVM
    {
        public string Name { get; set; }
        public string description { get; set; }
        public string Path { get; set; }
        public string salecode { get; set; }
    }
}
