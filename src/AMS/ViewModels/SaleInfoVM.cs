﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.ViewModels
{
    public class SaleInfoVM
    {
        public int Id { get; set; }
        public string Code { get; set; }
    
        public string InstitutionName { get; set; }
        public DateTime? Date { get; set; }
        public string DateString { get; set; }
        public string Description { get; set; }
        public decimal? TotalSales { get; set; }
        public decimal? CommissionRate { get; set; }
        public decimal? Commission { get; set; }
        public int? CollectionStatus { get; set; }
        public int? CollectionType { get; set; }
        public decimal? Fees { get; set; }
        public int? SupplyStatus { get; set; }
        public decimal? ValueAddedTax { get; set; }
        public DateTime? SupplyDate { get; set; }

        public string SupplyDateString { get; set; }

        public int SaleStatus { get; set; }

        public int? SaleType { get; set; }
        public int? ValueAddedStatus { get; set; }
        public DateTime? ValueAddedDate { get; set; }
        public string ValueAddedDateString { get; set; }

        public List<SaleFilesVM> files { get; set; }

        public List<SaleTemplatesVM> finalTemplates { get; set; }
        public List<TemplatesVM> Templates { get; set; }
    }

    public class SaleFilesVM
    {
        public int Id { get; set; }
        public string filename { get; set; }
        public string description { get; set; }
        public DateTime uploaddate { get; set; }
        public string FilePath { get; set; }
    }

    public class SaleTemplatesVM
    {
        public int Id { get; set; }
        public string filePath { get; set; }

        public DateTime uploaddate { get; set; }
    }

    public class TemplatesVM
    {
        public int Id { get; set; }
        public string filePath { get; set; }

        public DateTime uploaddate { get; set; }
    }
}
