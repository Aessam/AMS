﻿using AMS.Data.Repos.Interfaces;
using AMS.ViewModels.Evaluation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.Services
{
    public class EvaluationService
    {
        private readonly IEvaluationRepository _evaluationRepository;
        private readonly IEvaluationFilesRepository _evaluationFilesRepository;
        private readonly IEvaluationFinalTemplateRepository _evaluationFinalTemplateRepository;
        private readonly IEvaluationTemplateFileRepository _evaluationTemplateFileRepository;

        public EvaluationService(IEvaluationRepository evaluationRepository, IEvaluationFilesRepository evaluationFilesRepository, IEvaluationFinalTemplateRepository evaluationFinalTemplateRepository,IEvaluationTemplateFileRepository evaluationTemplateFileRepository)
        {
            _evaluationRepository = evaluationRepository;
            _evaluationFilesRepository = evaluationFilesRepository;
            _evaluationFinalTemplateRepository = evaluationFinalTemplateRepository;
            _evaluationTemplateFileRepository = evaluationTemplateFileRepository;
        }


        public List<EvaluationVM> GetAllEvaluationByDate(int year) {

            var data = _evaluationRepository.Filter(s => s.PreviewDate.Value.Year == year || s.PreviewDate == null).OrderByDescending(s => s.PreviewDate).Select(s =>
                 new EvaluationVM()
                 {
                     Id = s.Id,
                     Code = s.code,
                     ClientName = s.ClientName,

                     Date = s.PreviewDate,
                     status = (int)s.EvaluationStatus,

                     statusstring = (int)s.EvaluationStatus == 1 ? "مغلقة" : "مفتوحة"

                 }).ToList();
            return data;

        }
        public List<EvaluationVM> GetEvaluationBySearch(int type, int year, int month)
        {
            List<EvaluationVM> data = null;
            if (type == 0)
            {
                data = _evaluationRepository.Filter(s => s.PreviewDate.Value.Year == year && s.PreviewDate.Value.Month == month).OrderByDescending(s => s.PreviewDate).Select(s =>
                   new EvaluationVM()
                   {
                       Id = s.Id,
                       Code = s.code,
                       ClientName = s.ClientName,

                       Date = s.PreviewDate,
                       status = (int)s.EvaluationStatus,

                       statusstring = (int)s.EvaluationStatus == 1 ? "مغلقة" : "مفتوحة"
                   }).ToList();
            }
            else
            {
                data = _evaluationRepository.Filter(s => s.PreviewDate.Value.Year == year && s.PreviewDate.Value.Month == month && s.EvaluationType == type).OrderByDescending(s => s.PreviewDate).Select(s =>
                                     new EvaluationVM()
                                     {
                                         Id = s.Id,
                                         Code = s.code,
                                         ClientName = s.ClientName,

                                         Date = s.PreviewDate,
                                         
                                         status = (int)s.EvaluationStatus,

                                         statusstring = (int)s.EvaluationStatus == 1 ? "مغلقة" : "مفتوحة"
                                     }).ToList();
            }
            return data;
        }

        public EvaluationInfoVM GetInfoById(string evaluationcode)
        {
            EvaluationInfoVM data = new EvaluationInfoVM();
            var Maindata = _evaluationRepository.Filter(x => x.code == evaluationcode).FirstOrDefault();
            if (Maindata != null)
            {
                data.code = Maindata.code;
                data.Id = Maindata.Id;
                data.ClientName = Maindata.ClientName;
                data.PreviewDate = Maindata.PreviewDate;
                data.EvaluationType = Maindata.EvaluationType;
                data.CollectionStatus = Maindata.CollectionStatus;
                data.CollectionType = Maindata.CollectionType;
                data.Fees = Maindata.Fees;
                data.SupplyStatus = Maindata.SupplyStatus;
                data.ValueAddedTax = Maindata.ValueAddedTax;
                data.Supplydate = Maindata.Supplydate;
                data.generaltax = Maindata.generaltax;
                data.generalTaxStatus = Maindata.generalTaxStatus;
                data.EvaluationStatus = Maindata.EvaluationStatus;
                data.PreprationDate = Maindata.PreprationDate;
                data.files = new List<EvaluationFilesVM>();
                var files = _evaluationFilesRepository.Filter(f => f.EvalutionCode == evaluationcode);
                if (files != null)
                {
                    foreach (var item in files)
                    {
                        EvaluationFilesVM m = new EvaluationFilesVM();
                        m.Id = item.Id;
                        m.filename = item.filename;
                        m.description = item.description;
                        m.FilePath = item.FilePath;
                        m.uploaddate = item.uploaddate;
                        data.files.Add(m);
                    }
                }

                data.finalTemplates = new List<EvaluationTemplatesVM>();
                var temp = _evaluationFinalTemplateRepository.Filter(s => s.EvaluationCode == evaluationcode).ToList();
                foreach (var item in temp)
                {
                    EvaluationTemplatesVM t = new EvaluationTemplatesVM();
                    t.Id = item.Id;
                    t.uploaddate = item.uploaddate;
                    t.filePath = item.filePath;
                    data.finalTemplates.Add(t);
                }
                data.Templates = new List<ViewModels.TemplatesVM>();
                var template=_evaluationTemplateFileRepository.Filter(s => s.EvaluationCode == evaluationcode).ToList();
                foreach (var item in template)
                {
                    ViewModels.TemplatesVM t = new ViewModels.TemplatesVM();
                    t.Id = item.Id;
                    t.uploaddate = item.uploaddate;
                    t.filePath = item.filePath;
                    data.Templates.Add(t);
                }

            }
            return data;
        }


        public async Task CheckevaluationStatus(string evaluationcode)
        {
            var Data = _evaluationRepository.Filter(s => s.code == evaluationcode).FirstOrDefault();
            if (Data != null)
            {
                var finaltemplate = _evaluationFinalTemplateRepository.Filter(f => f.EvaluationCode == evaluationcode).Count();

                if (Data.PreprationDate != null && !string.IsNullOrEmpty(Data.ClientName) && Data.PreviewDate != null && Data.EvaluationType != null &&
                    Data.SupplyStatus != null && Data.Fees != null  && Data.ValueAddedTax != null
                    && Data.CollectionStatus == 1 && Data.CollectionType != null && (Data.generalTaxStatus != 0 && Data.generaltax==null) && Data.Supplydate != null && finaltemplate > 0)
                {
                    Data.EvaluationStatus = 1;
                }
                else
                { Data.EvaluationStatus = 0; }

                await _evaluationRepository.SaveAsync();
            }
        }
    }
}
