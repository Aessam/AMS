﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMS.Data.Repos.Interfaces;
using AMS.ViewModels;

namespace AMS.Services
{
    public class SaleServices
    {
        private readonly ISaleRepository _saleRepository;
        private readonly ISaleFileRepository _saleFileRepository;
        private readonly ISaleFinalTemplateRepository _saleFinalTemplateRepository;
        private readonly ISaleTemplateFileRepository _saleTemplateFileRepository;

        public SaleServices(ISaleRepository saleRepository, ISaleFileRepository saleFileRepository, ISaleFinalTemplateRepository saleFinalTemplateRepository,ISaleTemplateFileRepository saleTemplateFileRepository)
        {
            _saleRepository = saleRepository;
            _saleFileRepository = saleFileRepository;
            _saleFinalTemplateRepository = saleFinalTemplateRepository;
            _saleTemplateFileRepository = saleTemplateFileRepository;
        }

        public List<SaleVM> GetAllSalesByDate(int year)
        {
            var data = _saleRepository.Filter(s => s.Date.Value.Year == year || s.Date == null).OrderByDescending(s => s.Date).Select(s =>
                  new SaleVM()
                  {
                      Id = s.Id,
                      Code = s.Code,
                      Description = s.Description,
                      SaleName = s.InstitutionName,
                      date = s.Date,
                      status = (int)s.SaleStatus,

                      statusstring = (int)s.SaleStatus == 1 ? "مغلقة" : "مفتوحة"
                  }).ToList();
            return data;
        }

        public List<SaleVM> GetSalesBySearch(int type, int year, int month)
        {
            List<SaleVM> data = null;
            if (type == 0)
            {
                data = _saleRepository.Filter(s => s.Date.Value.Year == year && s.Date.Value.Month == month).OrderByDescending(s => s.Date).Select(s =>
                   new SaleVM()
                   {
                       Id = s.Id,
                       Code = s.Code,
                       Description = s.Description,
                       SaleName = s.InstitutionName,
                       date = s.Date,
                       status = (int)s.SaleStatus,

                       statusstring = (int)s.SaleStatus == 1 ? "مغلقة" : "مفتوحة"
                   }).ToList();
            }
            else
            {
                data = _saleRepository.Filter(s => s.Date.Value.Year == year && s.Date.Value.Month == month && s.SaleType == type).OrderByDescending(s => s.Date).Select(s =>
                                     new SaleVM()
                                     {
                                         Id = s.Id,
                                         Code = s.Code,
                                         Description = s.Description,
                                         SaleName = s.InstitutionName,
                                         date = s.Date,
                                         status = (int)s.SaleStatus,

                                         statusstring = (int)s.SaleStatus == 1 ? "مغلقة" : "مفتوحة"
                                     }).ToList();
            }
            return data;
        }

        public SaleInfoVM GetSaleInfoById(string salecode)
        {
            SaleInfoVM data = new SaleInfoVM();
            var Maindata = _saleRepository.Filter(x => x.Code == salecode).FirstOrDefault();
            if (Maindata != null)
            {
                data.Code = Maindata.Code;
                data.Id = Maindata.Id;
                data.InstitutionName = Maindata.InstitutionName;
                data.Date = Maindata.Date;
                data.Description = Maindata.Description;
                data.TotalSales = Maindata.TotalSales;
                data.CommissionRate = Maindata.CommissionRate;
                data.Commission = Maindata.Commission;
                data.CollectionStatus = Maindata.CollectionStatus;
                data.CollectionType = Maindata.CollectionType;
                data.Fees = Maindata.Fees;
                data.SupplyStatus = Maindata.SupplyStatus;
                data.ValueAddedTax = Maindata.ValueAddedTax;
                data.SupplyDate = Maindata.SupplyDate;
                data.SaleStatus = Maindata.SaleStatus;
                data.SaleType = Maindata.SaleType;
                data.ValueAddedStatus = Maindata.ValueAddedTaxStatus ;
                data.ValueAddedDate = Maindata.ValueAddedTaaxDate;

               

                data.files = new List<SaleFilesVM>();
                var salefiles = _saleFileRepository.Filter(f => f.SaleCode == salecode);
                if (salefiles != null)
                {
                    foreach (var item in salefiles)
                    {
                        SaleFilesVM m = new SaleFilesVM();
                        m.Id = item.Id;
                        m.filename = item.filename;
                        m.description = item.description;
                        m.FilePath = item.FilePath;
                        m.uploaddate = item.uploaddate;
                        data.files.Add(m);
                    }
                }

                data.finalTemplates = new List<SaleTemplatesVM>();
                var saletemp = _saleFinalTemplateRepository.Filter(s => s.SaleCode == salecode).ToList();
                foreach (var item in saletemp)
                {
                    SaleTemplatesVM t = new SaleTemplatesVM();
                    t.Id = item.Id;
                    t.uploaddate = item.uploaddate;
                    t.filePath = item.filePath;
                    data.finalTemplates.Add(t);
                }

                data.Templates = new List<TemplatesVM>();
                var templates = _saleTemplateFileRepository.Filter(s => s.SaleCode == salecode).ToList();
                foreach (var item in templates)
                {
                    TemplatesVM t = new TemplatesVM();
                    t.Id = item.Id;
                    t.uploaddate = item.uploaddate;
                    t.filePath = item.filePath;
                    data.Templates.Add(t);
                }

            }
            return data;
        }

        public async Task CheckSaleStatus(string salecode)
        {
            var saleData = _saleRepository.Filter(s => s.Code == salecode).FirstOrDefault();
            if (saleData != null)
            {
                var finaltemplate = _saleFinalTemplateRepository.Filter(f => f.SaleCode == salecode).Count();

                if (saleData.Date != null && !string.IsNullOrEmpty(saleData.InstitutionName) && saleData.SupplyDate != null && saleData.SaleType!=null &&
                    saleData.SupplyStatus != null && saleData.TotalSales != null && saleData.ValueAddedTaaxDate != null && saleData.ValueAddedTax != null 
                    && saleData.CollectionStatus == 1 && saleData.CollectionType != null && saleData.Commission != null && saleData.CommissionRate != null && finaltemplate > 0)
                {
                    saleData.SaleStatus = 1;
                }
                else
                { saleData.SaleStatus = 0; }

               await _saleRepository.SaveAsync();
            }
        }

        
    }
}
