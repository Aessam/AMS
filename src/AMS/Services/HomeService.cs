﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMS.Data.Repos.Interfaces;
using AMS.ViewModels;
using AMS.ViewModels.user;

namespace AMS.Services
{
    public class HomeService
    {
        private readonly IModuleRepository _moduleRepository;
        private readonly ISystemUserRepository _systemUserRepository;

        public HomeService(IModuleRepository moduleRepository, ISystemUserRepository systemUserRepository)
        {
            _moduleRepository = moduleRepository;
            _systemUserRepository = systemUserRepository;
        }

        public List<ModulesVm> GetModules()
        {
            var modulesList = _moduleRepository.GetAll().OrderBy(m => m.OrderNo)
                .Select(s => new ModulesVm() { Name = s.Name, logourl = s.LogoUrl, url = s.Url }).ToList();
            return modulesList;
        }

        public UserInfo ValidateLogin(string username, string password)
        {
            UserInfo info = null;
            var userInfo = _systemUserRepository.Filter(u => u.UserName == username & u.UserPassword == password).FirstOrDefault();
            if (userInfo != null)
            {
                info = new UserInfo();
                info.DisplayName = userInfo.DisplayName ?? string.Empty;
                info.UserId = userInfo.SystemUserId;
                info.UserName = userInfo.UserName;
                info.UserRole = userInfo.UserRole;
            }
            return info;
        }
    }
}
