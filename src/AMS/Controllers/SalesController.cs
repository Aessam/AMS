﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMS.Data.Repos.Interfaces;
using AMS.Services;
using Microsoft.AspNetCore.Mvc;
using AMS.Models;
using Microsoft.AspNetCore.Http;
using AMS.Utilities;
using System.Net.Http.Headers;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using AMS.ViewModels;
using System.Net.Http;

namespace AMS.Controllers
{
    public class SalesController : Controller
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ISaleRepository _saleRepository;
        private readonly ISaleFileRepository _saleFileRepository;
        private readonly ISaleFinalTemplateRepository _saleFinalTemplateRepository;
        private readonly ISaleTemplateFileRepository _saleTemplateFileRepository;
        private SaleServices _saleServices;
        private IHostingEnvironment hostingEnv;
        private GoogleServices _googleServices;
        private GenerateCode _generatecode;
        public SalesController(ISaleRepository saleRepository, IHttpContextAccessor httpContextAccessor, IHostingEnvironment env, ISaleFileRepository saleFileRepository, ISaleFinalTemplateRepository saleFinalTemplateRepository,ISaleTemplateFileRepository saleTemplateFileRepository)
        {
            _saleRepository = saleRepository;
            _saleFileRepository = saleFileRepository;
            _saleFinalTemplateRepository = saleFinalTemplateRepository;
      
            _httpContextAccessor = httpContextAccessor;
            _saleTemplateFileRepository = saleTemplateFileRepository;
            _saleServices = new SaleServices(_saleRepository, _saleFileRepository, _saleFinalTemplateRepository, _saleTemplateFileRepository);
            _googleServices = new GoogleServices();
            hostingEnv = env;
            _generatecode = new GenerateCode(_saleRepository,null);

        }

        public IActionResult Index()
        {
            var model = _saleServices.GetAllSalesByDate(DateTime.Now.Year);
            var lasttenyears = Enumerable
   .Range(0, 11)
   .Select(i => DateTime.Now.AddYears((i + 1) - 10))
   .Select(date => date.ToString("yyyy")).ToList();

            ViewBag.years = lasttenyears;
            return View(model);
        }
        [HttpGet]
        public IActionResult add_step1()
        {
            ViewBag.SaleCode = _generatecode.GenerateValidCode();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> add_Saledata(SaleModel saleModel)
        {

            if (saleModel != null)
            {
                try
                {
                    int userId = Convert.ToInt32(_httpContextAccessor.HttpContext.Session.GetString("userId"));
                    _saleRepository.Add(new Entities.Sale
                    {
                        InstitutionName = saleModel.InstitutionName,
                        SaleType = saleModel.SaleType,
                        Date = saleModel.Date,
                        Fees = saleModel.Fees,
                        CollectionStatus = saleModel.CollectionStatus,
                        CollectionType = saleModel.CollectionType,
                        SupplyDate = saleModel.SupplyDate,
                        SupplyStatus = saleModel.SupplyStatus,
                        TotalSales = saleModel.TotalSales,
                        CommissionRate = saleModel.CommissionRate,
                        Commission = saleModel.Commission,
                        ValueAddedTax = saleModel.ValueAddedTax,
                        SaleStatus = 0,
                        CreatedBy = userId,
                        CreationDate = DateTime.Now,
                        Code = saleModel.Code,
                        Description = saleModel.Description,
                        ValueAddedTaxStatus = saleModel.ValueAddedStatus,
                        ValueAddedTaaxDate = saleModel.ValueAddedDate


                    });

                    _saleRepository.Save();

                    await _saleServices.CheckSaleStatus(saleModel.Code);

                    return Redirect("/sales/");

                }
                catch (Exception e) { }


            }

            return View(saleModel);
        }

        //[HttpPost]
        //public IActionResult UploadFiles(IList<IFormFile> files)
        //{
        //    long size = 0;
        //    foreach (var file in files)
        //    {
        //        var filename = ContentDispositionHeaderValue
        //                        .Parse(file.ContentDisposition)
        //                        .FileName
        //                        .Trim('"');
        //        filename = filename + DateTime.Now.ToString();
        //        filename = hostingEnv.WebRootPath + $@"\Media\{filename}";
        //        size += file.Length;
        //        using (FileStream fs = System.IO.File.Create(filename))
        //        {
        //            file.CopyTo(fs);
        //            fs.Flush();
        //        }
        //    }
        //    ViewBag.Message = $"{files.Count} file(s) / { size}bytes uploaded successfully!";
        //    return View();
        //}

        [HttpPost]
        public IActionResult UploadFilesAjax()
        {
            try
            {
                long size = 0;
                var files = Request.Form.Files;
                string filepath = "";
                foreach (var file in files)
                {
                    var filename = ContentDispositionHeaderValue
                                    .Parse(file.ContentDisposition)
                                    .FileName
                                    .Trim('"');

                    string newFileName = Path.Combine(Path.GetDirectoryName(filename)
                , string.Concat(Path.GetFileNameWithoutExtension(filename)
                               , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                               , Path.GetExtension(filename)
                               )
                );


                    filename = hostingEnv.WebRootPath + $@"\Media\{newFileName}";
                    size += file.Length;
                    using (FileStream fs = System.IO.File.Create(filename))
                    {
                        file.CopyTo(fs);
                        fs.Flush();
                    }
                    filepath = filename;
                }
                string message = $"{files.Count} file(s) / { size} bytes uploaded successfully!";
                return Json(new { result = "true", filepath = filepath, date = DateTime.Now.ToShortDateString() });
            }
            catch (Exception e)
            {

                return Json(new { result = "false" });
            }

        }

        [HttpPost]
        public JsonResult AddSaleFile([FromBody]FileVM vm)
        {
            try
            {
                var salefiledetails = new Entities.SaleFiles
                {
                    filename = vm.Name,
                    FilePath = vm.Path,
                    description = vm.description,
                    uploaddate = DateTime.Now,
                    SaleCode = vm.salecode
                };
                _saleFileRepository.Add(salefiledetails);
                _saleFileRepository.Save();
                int id = salefiledetails.Id;
                return Json(new { result = "true", fileId = id.ToString() });

            }
            catch (Exception e)
            {

                return Json(new { result = "false" });
            }
        }

        [HttpPost]
        public JsonResult AddFinalSaleTemplate([FromBody]FileVM vm)
        {
            try
            {
                var tempdata = new Entities.SaleFinalTemplate
                {
                    filePath = vm.Path,

                    uploaddate = DateTime.Now,
                    SaleCode = vm.salecode
                };

                _saleFinalTemplateRepository.Add(tempdata);
                _saleFinalTemplateRepository.Save();

                int id = tempdata.Id;
                return Json(new { result = "true" ,fileId=id.ToString()});

            }
            catch (Exception e)
            {

                return Json(new { result = "false" });
            }
        }


        [HttpPost]
        public JsonResult AddSaleTemplate([FromBody]FileVM vm)
        {
            try
            {
                var tempdata = new Entities.SaleTemplateFile
                {
                    filePath = vm.Path,

                    uploaddate = DateTime.Now,
                    SaleCode = vm.salecode
                };

                _saleTemplateFileRepository.Add(tempdata);
                _saleTemplateFileRepository.Save();

                int id = tempdata.Id;
                return Json(new { result = "true", fileId = id.ToString() });

            }
            catch (Exception e)
            {

                return Json(new { result = "false" });
            }
        }

        [HttpPost]
        public JsonResult UploadAndOpenSaleTemplate(string salecode, string templatename)
        {
            try
            {
                var service = _googleServices.connecttogoogle();

                //uploadFile(service, @"E:\Ahmed essam.docx", "");
                string mimetype = _googleServices.GetMimeType(@"E:\نموذج كراسة شروط.doc");
                var file = _googleServices.insertFile(service, salecode + templatename + DateTime.Now.ToString(), "", "", mimetype, @"E:\نموذج كراسة شروط.doc");
                _googleServices.InsertPermission(service, file.Id, "", "anyone", "writer");


                return Json(new { result = "true", fileLink = file.AlternateLink });

            }
            catch (Exception)
            {
                return Json(new { result = "false" });
            }
        }


        [HttpPost]
        public async Task<JsonResult> DeleteSale(string salecode)
        {
            try
            {
                var saledata = _saleRepository.Filter(s => s.Code == salecode).FirstOrDefault();
                if (salecode != null)
                {
                    _saleRepository.Delete(saledata);

                    var salefiles = _saleFileRepository.Filter(f => f.SaleCode == salecode).ToList();
                    if (salefiles.Count > 0)
                    {
                        foreach (var item in salefiles)
                        {
                            _saleFileRepository.Delete(item);
                        }
                    }
                    var saleTemplateFiles = _saleFinalTemplateRepository.Filter(sf => sf.SaleCode == salecode).ToList();
                    if (saleTemplateFiles.Count > 0)
                    {
                        foreach (var item in saleTemplateFiles)
                        {
                            _saleFinalTemplateRepository.Delete(item);
                        }
                    }
                    await _saleRepository.SaveAsync();

                    return Json(new { result = "true" });
                }
                return Json(new { result = "false" });
            }
            catch (Exception)
            {

                return Json(new { result = "false" });
            }
        }


        [HttpPost]
        public async Task<JsonResult> DeleteSaleFile(int Id)
        {
            try
            {
                var salefile = _saleFileRepository.Filter(f => f.Id == Id).FirstOrDefault();
                if (salefile != null)
                {
                    _saleFileRepository.Delete(salefile);
                    _saleFileRepository.Save();
                    return Json(new { result = "true" });
                }
                return Json(new { result = "false" });

            }
            catch (Exception)
            {

                return Json(new { result = "false" });
            }
        }

        [HttpPost]
        public async Task<JsonResult> DeleteSaleFileTemp(int Id)
        {
            try
            {
                var salefile = _saleFinalTemplateRepository.Filter(f => f.Id == Id).FirstOrDefault();
                if (salefile != null)
                {
                    _saleFinalTemplateRepository.Delete(salefile);
                    _saleFinalTemplateRepository.Save();
                    return Json(new { result = "true" });
                }
                return Json(new { result = "false" });

            }
            catch (Exception)
            {

                return Json(new { result = "false" });
            }
        }

        [HttpPost]
        public async Task<JsonResult> DeleteTemp(int Id)
        {
            try
            {
                var salefile = _saleTemplateFileRepository.Filter(f => f.Id == Id).FirstOrDefault();
                if (salefile != null)
                {
                    _saleTemplateFileRepository.Delete(salefile);
                    _saleTemplateFileRepository.Save();
                    return Json(new { result = "true" });
                }
                return Json(new { result = "false" });

            }
            catch (Exception)
            {

                return Json(new { result = "false" });
            }
        }

        [HttpPost]
        public async Task<JsonResult> CloseSale(string salecode)
        {
            try
            {
                var sale = _saleRepository.Filter(s => s.Code == salecode).FirstOrDefault();
                if (sale != null)
                {
                    sale.SaleStatus = 1;
                }
                await _saleRepository.SaveAsync();

                return Json(new { result = "true" });
            }
            catch (Exception)
            {

                return Json(new { result = "false" });
            }

        }


        [HttpGet]
        public async Task<IActionResult> GetSaleSearchResult(int saletype, int year, int month)
        {
            var model = _saleServices.GetSalesBySearch(saletype, year, month);

            return PartialView("~/Views/Sales/_SalesList.cshtml", model);
        }

        [HttpGet]
        public async Task<IActionResult> EditSale(string salecode)
        {
            var model = _saleServices.GetSaleInfoById(salecode);

            ViewBag.SaleCode = salecode;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> EditSaledata(SaleModel saleModel)
        {

            if (saleModel != null)
            {
                try
                {
                    int userId = Convert.ToInt32(_httpContextAccessor.HttpContext.Session.GetString("userId"));

                    var saledata = _saleRepository.Filter(c => c.Code == saleModel.Code).FirstOrDefault();
                    if (saledata != null)
                    {
                        saledata.InstitutionName = saleModel.InstitutionName;
                        saledata.SaleType = saleModel.SaleType;
                        saledata.Date = saleModel.Date;
                        saledata.Fees = saleModel.Fees;
                        saledata.CollectionStatus = saleModel.CollectionStatus;
                        saledata.CollectionType = saleModel.CollectionType;
                        saledata.SupplyDate = saleModel.SupplyDate;
                        saledata.SupplyStatus = saleModel.SupplyStatus;
                        saledata.TotalSales = saleModel.TotalSales;
                        saledata.CommissionRate = saleModel.CommissionRate;
                        saledata.Commission = saleModel.Commission;
                        saledata.ValueAddedTax = saleModel.ValueAddedTax;

                        saledata.LastUpdatedBy = userId;
                        saledata.LastUpdatDate = DateTime.Now;

                        saledata.Description = saleModel.Description;
                        saledata.ValueAddedTaxStatus = saleModel.ValueAddedStatus;
                        saledata.ValueAddedTaaxDate = saleModel.ValueAddedDate;

                    }

                    _saleRepository.Save();

                    await _saleServices.CheckSaleStatus(saleModel.Code);

                }
                catch (Exception e)
                {


                }

            }
            return Redirect("/sales/");
        }

        [HttpGet]
        public async Task<IActionResult> ViewSale(string salecode)
        {
            var model = _saleServices.GetSaleInfoById(salecode);

            ViewBag.SaleCode = salecode;
            return View(model);
        }

        //[HttpPost]
        //public async Task<IActionResult> Download(string filename)
        //{
        //    try
        //    {
        //        //var fileName = filename;
        //        ////var filepath = $"wwwroot/docs/{fileName}";
        //        //string filepath = "http://dl.emdadgraphic.ir/upnew/Font-farsi/fontsfull.rar";
        //        //byte[] fileBytes = System.IO.File.ReadAllBytes(filepath);
        //        //return File(fileBytes, GetContentType(filename), fileName);

        //        var stream = new MemoryStream(System.Text.Encoding.ASCII.GetBytes("Hello World"));
        //        MediaTypeHeaderValue mediaTypeHeaderValue = new MediaTypeHeaderValue("text/plain");
        //        return new FileStreamResult(stream, mediaTypeHeaderValue.MediaType) { 
        //            FileDownloadName = "test.txt"
        //        };
        //    }
        //    catch (Exception e)
        //    {

        //        throw;
        //    }


        //}

        //private static HttpClient Client { get; } = new HttpClient();
        //[HttpPost]
        //public async Task<FileStreamResult> Download(string filename)
        //{
        //    var stream = await Client.GetStreamAsync("http://dl.emdadgraphic.ir/upnew/Font-farsi/fontsfull.rar");

        //    return new FileStreamResult(stream, new MediaTypeHeaderValue("application/x-rar-compressed").MediaType)
        //    {
        //        FileDownloadName = "file.rar"
        //    };
        //}

        //public async Task<IActionResult> Download(string filename)
        //{
        //    if (filename == null)
        //        return Content("filename not present");

        //    var path = Path.Combine(
        //                   Directory.GetCurrentDirectory(),
        //                   "wwwroot", filename);

        //    var memory = new MemoryStream();
        //    using (var stream = new FileStream(path, FileMode.Open))
        //    {
        //        await stream.CopyToAsync(memory);
        //    }
        //    memory.Position = 0;
        //    return File(memory, GetContentType(path), Path.GetFileName(path));
        //}

        //[HttpPost]
        //public  async Task<byte[]> DownloadFile(string filename)
        //{
        //    //string url = hostingEnv.WebRootPath + $@"\docs\{filename}";
        //    string url = "https://www.google.com/search?q=images&source=lnms&tbm=isch&sa=X&ved=0ahUKEwi8ztyn3YDeAhXNLFAKHehqBHAQ_AUIDigB&biw=1242&bih=597#imgdii=NxOKIUebXwWkrM:&imgrc=A6JJqffgz3xzlM:";
        //    using (var client = new HttpClient())
        //    {
        //        try
        //        {
        //            using (var result = await client.GetAsync(url))
        //            {
        //                if (result.IsSuccessStatusCode)
        //                {
        //                    return await result.Content.ReadAsByteArrayAsync();
        //                }

        //            }
        //        }
        //        catch (Exception e)
        //        {

        //            throw;
        //        }

        //    }
        //    return null;
        //}


        //private string GetContentType(string path)
        //{
        //    var types = GetMimeTypes();
        //    var ext = Path.GetExtension(path).ToLowerInvariant();
        //    return types[ext];
        //}

        //private Dictionary<string, string> GetMimeTypes()
        //{
        //    return new Dictionary<string, string>
        //    {
        //        {".txt", "text/plain"},
        //        {".pdf", "application/pdf"},
        //        {".doc", "application/vnd.ms-word"},
        //        {".docx", "application/vnd.ms-word"},
        //        {".xls", "application/vnd.ms-excel"},
        //        {".xlsx", "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet"},
        //        {".png", "image/png"},
        //        {".jpg", "image/jpeg"},
        //        {".jpeg", "image/jpeg"},
        //        {".gif", "image/gif"},
        //        {".csv", "text/csv"},
        //        { ".rar","application/x-rar-compressed"}
        //    };
        //}
    }
}