﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AMS.Models;
using Microsoft.AspNetCore.Http;
using AMS.Services;
using AMS.Data.Repos.Interfaces;
using AMS.ViewModels.user;
using AMS.Utilities;

namespace AMS.Controllers
{
    public class HomeController : Controller
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private HomeService _homeService;
        private readonly IModuleRepository _moduleRepository;
        private readonly ISystemUserRepository _systemUserRepository;
        public HomeController(IModuleRepository moduleRepository, ISystemUserRepository systemUserRepository,IHttpContextAccessor httpContextAccessor)
        {
            _moduleRepository = moduleRepository;
            _systemUserRepository = systemUserRepository;
            _httpContextAccessor = httpContextAccessor;
            _homeService = new HomeService(_moduleRepository, _systemUserRepository);
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public IActionResult SignIn()
        {
            return View();
        }

        [HttpPost]
        public IActionResult SignIn(IFormCollection fc)
        {

            string username = fc["username"].ToString();
            string Password = fc["password"].ToString();

            UserInfo userinfo = _homeService.ValidateLogin(username, Password);
            if (userinfo != null)
            {
                SessionUser.SetUserDataSession(_httpContextAccessor, userinfo);
                return Redirect("/homepage/");
            }
            else
            {
                return Redirect("/signin/");
            }

            
        }

        public IActionResult HomePage()
        {
            var modules = _homeService.GetModules();
            return View(modules);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
