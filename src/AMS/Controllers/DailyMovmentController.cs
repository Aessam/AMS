﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using AMS.Data.Repos.Interfaces;
using AMS.ViewModels.DailyMovment;

namespace AMS.Controllers
{
    public class DailyMovmentController : Controller
    {

        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IDailyMovmentRepository _dailyMovmentRepository;

        public DailyMovmentController(IHttpContextAccessor httpContextAccessor, IDailyMovmentRepository dailyMovmentRepository)
        {
            _httpContextAccessor = httpContextAccessor;
            _dailyMovmentRepository = dailyMovmentRepository;
        }

        public IActionResult Index()
        {

            var data = _dailyMovmentRepository.Filter(d => d.MovmentDate.Date == DateTime.Now.Date).ToList();

            return View(data);
        }

        [HttpPost]
        public IActionResult AddMovments(dailymovmentVM model)
        {
            if (model != null)
            {
                try
                {
                    int userId = Convert.ToInt32(_httpContextAccessor.HttpContext.Session.GetString("userId"));
                    Entities.DailyMovments movment = new Entities.DailyMovments();
                    movment.MovmentDetails = model.movmentdetails;
                    movment.MovmentDate = model.movmentdate;
                    movment.MovmentTime = model.movmenttime;
                    movment.CreatedBy = userId;
                    movment.CreationDate = DateTime.Now;
                    _dailyMovmentRepository.Add(movment);

                    _dailyMovmentRepository.Save();



                    return Json(new {status=1 ,id=movment.Id});

                }
                catch (Exception e) { return Json(new { status = 0 }); }


            }

            return Json(new { status = 0 });
        }

        [HttpPost]
        public IActionResult RemoveMovment(int id)
        {
            var movment = _dailyMovmentRepository.Filter(m => m.Id == id).FirstOrDefault();
            if (movment != null)
                _dailyMovmentRepository.Delete(movment);

            _dailyMovmentRepository.Save();

            return Json(new { status = 1 });
        }

        [HttpGet]
        public IActionResult GetMovmentsbyDate(DateTime date)
        {

            var data= _dailyMovmentRepository.Filter(d => d.MovmentDate.Date == date.Date).ToList();

            return PartialView("~/Views/DailyMovment/_dailyMovmentList.cshtml", data);
        }
    }
}