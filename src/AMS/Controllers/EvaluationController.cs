﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using AMS.Data.Repos.Interfaces;
using AMS.Services;
using AMS.Utilities;
using AMS.Models;
using System.Net.Http.Headers;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using AMS.ViewModels;

namespace AMS.Controllers
{
    public class EvaluationController : Controller
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IEvaluationRepository _evaluationRepository;
        private readonly IEvaluationFilesRepository _evaluationFileRepository;
        private readonly IEvaluationFinalTemplateRepository _evaluationFinalTemplateRepository;
        private readonly IEvaluationTemplateFileRepository _evaluationTemplateFileRepository;
        private EvaluationService _evaluationServices;
        private GenerateCode _generatecode;
        private IHostingEnvironment hostingEnv;

        public EvaluationController(IHttpContextAccessor httpContextAccessor, IEvaluationRepository evaluationRepository, IHostingEnvironment env, IEvaluationFilesRepository evaluationFilesRepository, IEvaluationFinalTemplateRepository evaluationFinalTemplateRepository,IEvaluationTemplateFileRepository evaluationTemplateFileRepository)
        {
            _httpContextAccessor = httpContextAccessor;
            _evaluationFileRepository = evaluationFilesRepository;
            _evaluationRepository = evaluationRepository;
            _evaluationFinalTemplateRepository = evaluationFinalTemplateRepository;
            _evaluationTemplateFileRepository = evaluationTemplateFileRepository;
            hostingEnv = env;
            _evaluationServices = new EvaluationService(_evaluationRepository, _evaluationFileRepository, _evaluationFinalTemplateRepository,_evaluationTemplateFileRepository);
            
            _generatecode = new GenerateCode(null, _evaluationRepository);
        }




        public IActionResult Index()
        {

            var model = _evaluationServices.GetAllEvaluationByDate(DateTime.Now.Year);
            var lasttenyears = Enumerable
   .Range(0, 11)
   .Select(i => DateTime.Now.AddYears((i + 1) - 10))
   .Select(date => date.ToString("yyyy")).ToList();

            ViewBag.years = lasttenyears;

            return View(model);
        }


        [HttpPost]
        public async Task<JsonResult> DeleteEvaluation(string code)
        {
            try
            {
                var data = _evaluationRepository.Filter(s => s.code == code).FirstOrDefault();
                if (data != null)
                {
                    _evaluationRepository.Delete(data);

                    var files = _evaluationFileRepository.Filter(f => f.EvalutionCode == code).ToList();
                    if (files.Count > 0)
                    {
                        foreach (var item in files)
                        {
                            _evaluationFileRepository.Delete(item);
                        }
                    }
                    var TemplateFiles = _evaluationFinalTemplateRepository.Filter(sf => sf.EvaluationCode == code).ToList();
                    if (TemplateFiles.Count > 0)
                    {
                        foreach (var item in TemplateFiles)
                        {
                            _evaluationFinalTemplateRepository.Delete(item);
                        }
                    }
                    await _evaluationRepository.SaveAsync();

                    return Json(new { result = "true" });
                }
                return Json(new { result = "false" });
            }
            catch (Exception)
            {

                return Json(new { result = "false" });
            }
        }


        [HttpGet]
        public async Task<IActionResult> GetEvaluationSearchResult(int evaltype, int year, int month)
        {
            var model = _evaluationServices.GetEvaluationBySearch(evaltype, year, month);

            return PartialView("~/Views/Evaluation/_EvaluationList.cshtml", model);
        }

        [HttpGet]
        public IActionResult add_step1()
        {
            ViewBag.EvaluationCode = _generatecode.GenerateValidEvaluationCode();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> add_Evaluationdata(EvaluationModel model)
        {

            if (model != null)
            {
                try
                {
                    int userId = Convert.ToInt32(_httpContextAccessor.HttpContext.Session.GetString("userId"));
                    _evaluationRepository.Add(new Entities.Evaluations.Evaluation
                    {
                        ClientName = model.ClientName,
                        EvaluationType = model.EvaluationType,
                        PreprationDate = model.PreprationDate,
                        PreviewDate = model.PreviewDate,
                        Fees = model.Fees,
                        CollectionStatus = model.CollectionStatus,
                        CollectionType = model.CollectionType,
                        Supplydate = model.Supplydate,
                        SupplyStatus = model.SupplyStatus,
                        generaltax = model.generaltax,
                        generalTaxStatus = model.generalTaxStatus,
                        ValueAddedTax = model.ValueAddedTax,

                        CreatedBy = userId,
                        CreationDate = DateTime.Now,
                        code = model.code,


                    });

                    _evaluationRepository.Save();

                    await _evaluationServices.CheckevaluationStatus(model.code);

                    return Redirect("/evaluations/");

                }
                catch (Exception e) { }


            }

            return View(model);
        }

        [HttpPost]
        public IActionResult UploadFilesAjax()
        {
            try
            {
                long size = 0;
                var files = Request.Form.Files;
                string filepath = "";
                foreach (var file in files)
                {
                    var filename = ContentDispositionHeaderValue
                                    .Parse(file.ContentDisposition)
                                    .FileName
                                    .Trim('"');

                    string newFileName = Path.Combine(Path.GetDirectoryName(filename)
                , string.Concat(Path.GetFileNameWithoutExtension(filename)
                               , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                               , Path.GetExtension(filename)
                               )
                );


                    filename = hostingEnv.WebRootPath + $@"\Media\{newFileName}";
                    size += file.Length;
                    using (FileStream fs = System.IO.File.Create(filename))
                    {
                        file.CopyTo(fs);
                        fs.Flush();
                    }
                    filepath = filename;
                }
                string message = $"{files.Count} file(s) / { size} bytes uploaded successfully!";
                return Json(new { result = "true", filepath = filepath, date = DateTime.Now.ToShortDateString() });
            }
            catch (Exception e)
            {

                return Json(new { result = "false" });
            }

        }

        [HttpPost]
        public async Task<JsonResult> CloseEvaluation(string code)
        {
            try
            {
                var sale = _evaluationRepository.Filter(s => s.code == code).FirstOrDefault();
                if (sale != null)
                {
                    sale.EvaluationStatus = 1;
                }
                await _evaluationRepository.SaveAsync();

                return Json(new { result = "true" });
            }
            catch (Exception)
            {

                return Json(new { result = "false" });
            }

        }
        [HttpPost]
        public JsonResult AddEvaluationFile([FromBody]FileVM vm)
        {
            try
            {
                var salefiledetails = new Entities.Evaluations.EvaluationFile
                {
                    filename = vm.Name,
                    FilePath = vm.Path,
                    description = vm.description,
                    uploaddate = DateTime.Now,
                    EvalutionCode = vm.salecode
                };
                _evaluationFileRepository.Add(salefiledetails);
                _evaluationRepository.Save();
                int id = salefiledetails.Id;
                return Json(new { result = "true", fileId = id.ToString() });

            }
            catch (Exception e)
            {

                return Json(new { result = "false" });
            }
        }

        [HttpPost]
        public JsonResult AddFinalEvaluationTemplate([FromBody]FileVM vm)
        {
            try
            {
                var tempdata = new Entities.Evaluations.EvaluationFinalTemplate
                {
                    filePath = vm.Path,

                    uploaddate = DateTime.Now,
                    EvaluationCode = vm.salecode
                };

                _evaluationFinalTemplateRepository.Add(tempdata);
                _evaluationFinalTemplateRepository.Save();

                int id = tempdata.Id;
                return Json(new { result = "true", fileId = id.ToString() });

            }
            catch (Exception e)
            {

                return Json(new { result = "false" });
            }
        }

        [HttpPost]
        public async Task<JsonResult> DeleteEvaluationFile(int Id)
        {
            try
            {
                var salefile = _evaluationFileRepository.Filter(f => f.Id == Id).FirstOrDefault();
                if (salefile != null)
                {
                    _evaluationFileRepository.Delete(salefile);
                    _evaluationFileRepository.Save();
                    return Json(new { result = "true" });
                }
                return Json(new { result = "false" });

            }
            catch (Exception)
            {

                return Json(new { result = "false" });
            }
        }

        [HttpPost]
        public async Task<JsonResult> DeleteEvaluationFileTemp(int Id)
        {
            try
            {
                var salefile = _evaluationFinalTemplateRepository.Filter(f => f.Id == Id).FirstOrDefault();
                if (salefile != null)
                {
                    _evaluationFinalTemplateRepository.Delete(salefile);
                    _evaluationFinalTemplateRepository.Save();
                    return Json(new { result = "true" });
                }
                return Json(new { result = "false" });

            }
            catch (Exception)
            {

                return Json(new { result = "false" });
            }
        }

        [HttpGet]
        public async Task<IActionResult> EditEvaluation(string evaluationcode)
        {
            var model = _evaluationServices.GetInfoById(evaluationcode);

            ViewBag.EvaluationCode = evaluationcode;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> EditEvaluationdata(EvaluationModel Model)
        {

            if (Model != null)
            {
                try
                {
                    int userId = Convert.ToInt32(_httpContextAccessor.HttpContext.Session.GetString("userId"));

                    var saledata = _evaluationRepository.Filter(c => c.code == Model.code).FirstOrDefault();
                    if (saledata != null)
                    {
                        saledata.ClientName = Model.ClientName;
                        saledata.EvaluationType = Model.EvaluationType;
                        saledata.PreviewDate = Model.PreviewDate;
                        saledata.Fees = Model.Fees;
                        saledata.CollectionStatus = Model.CollectionStatus;
                        saledata.CollectionType = Model.CollectionType;
                        saledata.Supplydate = Model.Supplydate;
                        saledata.SupplyStatus = Model.SupplyStatus;
                        saledata.PreprationDate = Model.PreprationDate;
                        saledata.generalTaxStatus = Model.generalTaxStatus;
                        saledata.generaltax = Model.generaltax;
                        saledata.ValueAddedTax = Model.ValueAddedTax;

                        saledata.LastUpdatedBy = userId;
                        saledata.LastUpdatDate = DateTime.Now;



                    }

                    _evaluationRepository.Save();

                    await _evaluationServices.CheckevaluationStatus(Model.code);

                }
                catch (Exception e)
                {


                }

            }
            return Redirect("/evaluations/");
        }

        [HttpGet]
        public async Task<IActionResult> ViewEvaluation(string evaluationcode)
        {
            var model = _evaluationServices.GetInfoById(evaluationcode);

            ViewBag.EvaluationCode = evaluationcode;
            return View(model);
        }

        [HttpPost]
        public async Task<JsonResult> DeleteTemp(int Id)
        {
            try
            {
                var salefile = _evaluationTemplateFileRepository.Filter(f => f.Id == Id).FirstOrDefault();
                if (salefile != null)
                {
                    _evaluationTemplateFileRepository.Delete(salefile);
                    _evaluationTemplateFileRepository.Save();
                    return Json(new { result = "true" });
                }
                return Json(new { result = "false" });

            }
            catch (Exception)
            {

                return Json(new { result = "false" });
            }
        }

        [HttpPost]
        public JsonResult AddEvaluationTemplate([FromBody]FileVM vm)
        {
            try
            {
                var tempdata = new Entities.Evaluations.EvaluationTemplateFile
                {
                    filePath = vm.Path,

                    uploaddate = DateTime.Now,
                    EvaluationCode = vm.salecode
                };

                _evaluationTemplateFileRepository.Add(tempdata);
                _evaluationTemplateFileRepository.Save();

                int id = tempdata.Id;
                return Json(new { result = "true", fileId = id.ToString() });

            }
            catch (Exception e)
            {

                return Json(new { result = "false" });
            }
        }


    }
}