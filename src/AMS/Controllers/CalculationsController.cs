﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AMS.ViewModels;
using Microsoft.AspNetCore.Http;
using AMS.Data.Repos.Interfaces;
using AMS.ViewModels.Calculation;
using Newtonsoft.Json;

namespace AMS.Controllers
{
    public class CalculationsController : Controller
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ILotRepository _lotRepository;
        private readonly ISaleRepository _saleRepository;
        private readonly ILot_BuyersRepository _lot_BuyersRepository;
        private readonly ICustomerRepository _customerRepository;
        private readonly IMixedCalculationRepository _mixedCalculationRepository;
        private readonly IMixedCalculation_LotsRepository _mixedCalculation_LotsRepository;
        private readonly IMixedCalculation_transactionsRepository _mixedCalculation_TransactionsRepository;
        public CalculationsController(IHttpContextAccessor httpContextAccessor, ILotRepository lotRepository,
            ISaleRepository saleRepository, ILot_BuyersRepository lot_BuyersRepository, ICustomerRepository customerRepository,
            IMixedCalculationRepository mixedCalculationRepository, IMixedCalculation_LotsRepository mixedCalculation_LotsRepository,IMixedCalculation_transactionsRepository mixedCalculation_TransactionsRepository)
        {
            _httpContextAccessor = httpContextAccessor;
            _lotRepository = lotRepository;
            _saleRepository = saleRepository;
            _lot_BuyersRepository = lot_BuyersRepository;
            _customerRepository = customerRepository;
            _mixedCalculationRepository = mixedCalculationRepository;
            _mixedCalculation_LotsRepository = mixedCalculation_LotsRepository;
            _mixedCalculation_TransactionsRepository = mixedCalculation_TransactionsRepository;
        }
        public async Task<IActionResult> Index()
        {


            var model = await _lotRepository.getall();
            var serliazemodel = JsonConvert.SerializeObject(model);

            List<calculationsale> salemodel = new List<calculationsale>();
            salemodel = JsonConvert.DeserializeObject<List<calculationsale>>(serliazemodel);
            ViewBag.Items = model;
            return View(salemodel);
        }

        public IActionResult Add_Lot()
        {
            AddLotVM model = new AddLotVM();
            ViewBag.sales = _saleRepository.Filter(s => s.SaleStatus == 0).ToList();
            model.customers = _customerRepository.GetAll().Select(c => new buyersObject { ID = c.CustomerId, Name = c.Name }).ToList();
            return View(model);
        }

        [HttpPost]
        public IActionResult Add_Lot(AddLotVM model, IFormCollection fc)
        {
            try
            {
                List<int> saleID = fc["SalesList"].ToString().Split(',').Select(Int32.Parse).ToList();
                List<int> customerIDs = fc["CustomersList"].ToString().Split(',').Select(Int32.Parse).ToList();
                Entities.Calculations.Lot lot = new Entities.Calculations.Lot();
                lot.LotNumber = model.LotNumber;
                lot.LotType = model.LotType;
                lot.WeightorNumber = model.Weightornumber;
                lot.priceperweightornumber = model.priceperweightornumber;
                lot.BuyValue = model.BuyValue;
                lot.FK_SaleID = saleID.First();
                lot.saleId = saleID.First();
                lot.Paidamount = model.paidamount;
                lot.FeesPercentage = model.FeesPercentage;
                lot.InsurancePercentage = model.InsurancePercentage;

                decimal total = (model.FeesPercentage / 100) * model.BuyValue + (model.InsurancePercentage / 100) * model.BuyValue + model.BuyValue;
                decimal reminder = total - model.paidamount;

                lot.Total = total;
                lot.Reminder = reminder;
                _lotRepository.Add(lot);
                _lotRepository.Save();

                int Lotid = lot.LotId;

                foreach (var item in customerIDs)
                {
                    _lot_BuyersRepository.Add(new Entities.Calculations.Lot_Buyers
                    {
                        LotId = Lotid,
                        CustomerId = item
                    });
                }
                _lot_BuyersRepository.Save();

                return Redirect("/calculations/");
            }
            catch (Exception)
            {

                return View(model);
            }



        }


        [HttpGet]
        public async Task<IActionResult> Sale_LotDetails(int saleId)
        {
            var data = await _lotRepository.getsalelotdetails(saleId);
            var serliazemodel = JsonConvert.SerializeObject(data);

            List<Sale_LotdetailsVM> datamodel = new List<Sale_LotdetailsVM>();
            datamodel = JsonConvert.DeserializeObject<List<Sale_LotdetailsVM>>(serliazemodel);

            foreach (var item in datamodel)
            {
                List<int> custIds = item.buyers.Select(b => b.CustomerId).ToList();

                List<string> customersnamesList = _customerRepository.Filter(c => custIds.Contains(c.CustomerId)).Select(c => c.Name).ToList();
                item.BuyersName = String.Join(",", customersnamesList);
                item.BuyValueWithFees = item.BuyValue + (item.FeesPercentage/100 * item.BuyValue);
                item.InsuranceValue = item.InsurancePercentage * item.BuyValue;
            }
            ViewBag.saleTitle = _saleRepository.Filter(s => s.Id == saleId).Select(s => s.InstitutionName).FirstOrDefault();
            return View(datamodel);
        }

        [HttpGet]
        public IActionResult MixedCalculations(int saleId)
        {

            var model = _mixedCalculationRepository.Filter(m => m.SaleId == saleId).ToList();
            ViewBag.saleID = saleId;
            ViewBag.saleTitle = _saleRepository.Filter(s => s.Id == saleId).Select(s => s.InstitutionName).FirstOrDefault();
            return View(model);

        }

        [HttpGet]
        public async Task<IActionResult> MixedCalculation_Add(int saleId)
        {
            List<int> usedlots = await _mixedCalculationRepository.GetUsedLots(saleId);
            ViewBag.saleID = saleId;
            ViewBag.Lots = _lotRepository.Filter(l => !usedlots.Contains(l.LotId) && l.FK_SaleID == saleId).ToList();
            ViewBag.saleTitle = _saleRepository.Filter(s => s.Id == saleId).Select(s => s.InstitutionName).FirstOrDefault();
            return View();
        }

        [HttpPost]
        public ActionResult MixedCalculation_Add(IFormCollection fc)
        {
            List<int> LotIDs = fc["CustomersList"].ToString().Split(',').Select(Int32.Parse).ToList();
            int saleId = Convert.ToInt32(fc["saleID"].ToString());

            decimal TotalValue = _lotRepository.Filter(l => LotIDs.Contains(l.LotId)).Select(l => l.Total).Sum();

            Entities.Calculations.MixedCalculation Calculationobject = new Entities.Calculations.MixedCalculation();
            Calculationobject.Isclosed = false;
            Calculationobject.Name = fc["accountname"].ToString();
            Calculationobject.TotalPaid = 0;
            Calculationobject.TotalValue = TotalValue;
            Calculationobject.SaleId = saleId;


            _mixedCalculationRepository.Add(Calculationobject);
            foreach (var item in LotIDs)
            {
                _mixedCalculation_LotsRepository.Add(new Entities.Calculations.MixedCalculation_Lots
                {
                    LotId = item,
                    MixedCalculationId = Calculationobject.Id
                });
            }
            _mixedCalculationRepository.Save();

            return Redirect("/calculations/Mixed/" + saleId);

        }

        [HttpGet]
        public async Task<ActionResult> MixedCalculation_details(int saleId, int mixedcalcId)
        {

            List<int> calculationLots = _mixedCalculation_LotsRepository.Filter(m => m.MixedCalculationId == mixedcalcId).Select(m => m.LotId).ToList();
            var data = await _lotRepository.getsalelotdetails(calculationLots);
            var serliazemodel = JsonConvert.SerializeObject(data);

            List<Sale_LotdetailsVM> datamodel = new List<Sale_LotdetailsVM>();
            datamodel = JsonConvert.DeserializeObject<List<Sale_LotdetailsVM>>(serliazemodel);

            foreach (var item in datamodel)
            {
                List<int> custIds = item.buyers.Select(b => b.CustomerId).ToList();

                List<string> customersnamesList = _customerRepository.Filter(c => custIds.Contains(c.CustomerId)).Select(c => c.Name).ToList();
                item.BuyersName = String.Join(",", customersnamesList);
                item.BuyValueWithFees = item.BuyValue + (item.FeesPercentage/100 * item.BuyValue);
                item.InsuranceValue = item.InsurancePercentage * item.BuyValue;
            }
            ViewBag.saleTitle = _saleRepository.Filter(s => s.Id == saleId).Select(s => s.InstitutionName).FirstOrDefault();
            ViewBag.accountId = mixedcalcId;
            var mixedCalc= _mixedCalculationRepository.Filter(m => m.Id == mixedcalcId).FirstOrDefault();
            ViewBag.isclosed = mixedCalc.Isclosed;
            decimal TotalReminder = datamodel.Sum(d => d.Reminder);
            List<TransactionVM> transactions = new List<TransactionVM>();
            transactions.Add(new TransactionVM { MixedCalculationId = mixedcalcId, PaidDescription = "الاجمالى", Paidamount = TotalReminder ,type=string.Empty });

            transactions.AddRange(_mixedCalculation_TransactionsRepository.Filter(t => t.MixedCalculationId == mixedcalcId).Select(t => new TransactionVM {MixedCalculationId=t.MixedCalculationId,PaidDescription=t.summary,Paidamount=t.Value ,type=t.type }));

            var totalaftertransaction = TotalReminder - (_mixedCalculation_TransactionsRepository.Filter(t => t.MixedCalculationId == mixedcalcId).Select(t => t.Value).Sum());
            transactions.Add(new TransactionVM { MixedCalculationId = mixedcalcId, PaidDescription = "الباقى", Paidamount = totalaftertransaction,type=string.Empty });
            ViewData["transactions"] = transactions;
            return View(datamodel);
        }

        [HttpPost]
        public async Task<ActionResult> AddCalculation_Transaction([FromBody]TransactionVM body)
        {
            try
            {
                _mixedCalculation_TransactionsRepository.Add(new Entities.Calculations.MixedCalculation_Transactions
                {
                    MixedCalculationId = body.MixedCalculationId,
                    Value = body.Paidamount,
                    summary = body.PaidDescription,
                    time = DateTime.Now,
                    type="تحصيل"
                });

                var mixedcalculation = _mixedCalculationRepository.Filter(m => m.Id == body.MixedCalculationId).FirstOrDefault();

                mixedcalculation.TotalPaid += body.Paidamount;

                if (mixedcalculation.TotalPaid == mixedcalculation.TotalValue)
                    mixedcalculation.Isclosed = true;

                _mixedCalculationRepository.Save();

                return Json(new {success="1" });

            }
            catch (Exception)
            {

                return Json(new { success = "0" });
            }
            
        }

    }
}