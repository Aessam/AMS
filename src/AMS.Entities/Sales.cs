﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AMS.Entities
{
    public class Sale:BaseEntity
    {
        public int Id { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public string InstitutionName { get; set; }
        public DateTime? Date { get; set; }
        public string Description { get; set; }
        public decimal? TotalSales { get; set; }
        public decimal? CommissionRate { get; set; }
        public decimal? Commission { get; set; }
        public int? CollectionStatus { get; set; }
        public int? CollectionType { get; set; }
        public decimal? Fees { get; set; }
        public int? SupplyStatus { get; set; }
        public decimal? ValueAddedTax { get; set; }
        public DateTime? SupplyDate { get; set; }

        public int? ValueAddedTaxStatus { get; set; }
        public DateTime? ValueAddedTaaxDate { get; set; }

        public int SaleStatus { get; set; }

        public int? SaleType { get; set; }

       


    }

    public enum CollectionStatus
    {
        Done = 1,
        Notdone
    }
    public enum CollectionTypes
    {
        Cash = 1,
        Check,
        Transfer
    }

    public enum SaleStatus
    {
        completed=1,
        notcompleted
    }
}
