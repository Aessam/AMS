﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AMS.Entities.Evaluations
{
   public class EvaluationFile
    {
        [Key]
        public int Id { get; set; }
        public string filename { get; set; }
        public string description { get; set; }
        public string EvalutionCode { get; set; }
        public DateTime uploaddate { get; set; }
        public string FilePath { get; set; }
    }
}
