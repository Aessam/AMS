﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AMS.Entities.Evaluations
{
   public class EvaluationFinalTemplate
    {
        [Key]
        public int Id { get; set; }

        public string EvaluationCode { get; set; }

        public string filePath { get; set; }

        public DateTime uploaddate { get; set; }
    }
}
