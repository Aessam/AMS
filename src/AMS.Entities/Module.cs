﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMS.Entities
{
    public class Module
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LogoUrl { get; set; }
        public string Url { get; set; }
        public int OrderNo { get; set; }

    }
}
