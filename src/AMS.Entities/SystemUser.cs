﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AMS.Entities
{
   public class SystemUser
    {
        [Key]
        public int SystemUserId { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public int  UserRole { get; set; }
        [Required]
        public string UserPassword { get; set; }

        public string DisplayName { get; set; }



    }
}
