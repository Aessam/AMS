﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AMS.Entities
{
    public class DailyMovments:BaseEntity
    {
        [Key]
        public int Id { get; set; }

        public string MovmentDetails { get; set; }

        public DateTime MovmentDate { get; set; }

        public TimeSpan MovmentTime { get; set; }

    }
}
