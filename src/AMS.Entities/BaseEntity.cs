﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMS.Entities
{
    public class BaseEntity
    {
        public long CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }

        public long? LastUpdatedBy { get; set; }
        public DateTime? LastUpdatDate { get; set; }
    }
}
