﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AMS.Entities.Calculations
{
  public  class MixedCalculation_Lots
    {
        [Key]
        public int Id { get; set; }

        public int MixedCalculationId { get; set; }
        public MixedCalculation MixedCalculation  { get; set; }
        public int LotId { get; set; }
        public Lot lot { get; set; }
    }
}
