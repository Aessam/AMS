﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AMS.Entities.Calculations
{
    public class Lot_Buyers
    {
        [Key]
        public int Lot_BuyersId { get; set; }
        public int LotId { get; set; }
        public int CustomerId { get; set; }
        public Lot lot { get; set; }
        public Lot_Buyers lot_buyers { get; set; }
    }
}
