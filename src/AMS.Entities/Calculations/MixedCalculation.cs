﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AMS.Entities.Calculations
{
    public class MixedCalculation
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int SaleId { get; set; }
        public Sale sale { get; set; }
        public decimal TotalValue { get; set; }
        public decimal TotalPaid { get; set; }
        public bool Isclosed { get; set; }
    }
}
