﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AMS.Entities.Calculations
{
    public class Lot
    {
        [Key]
        public int LotId { get; set; }

        public string LotNumber { get; set; }
        public int FK_SaleID { get; set; }
        public string LotType { get; set; }
        
        public decimal BuyValue { get; set; }
        public int WeightorNumber { get; set; }
        public decimal priceperweightornumber { get; set; }
        public decimal Paidamount { get; set; }
        public decimal InsurancePercentage { get; set; }
        public decimal FeesPercentage { get; set; }
        public decimal Total { get; set; }
        public decimal Reminder { get; set; }
        public Sale sale { get; set; }
        public int saleId { get; set; }
        public List<Lot_Buyers> Buyers { get; set; }

    }
}
