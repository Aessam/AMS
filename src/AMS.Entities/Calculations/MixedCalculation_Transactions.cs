﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AMS.Entities.Calculations
{
   public class MixedCalculation_Transactions
    {
        [Key]
        public int Id { get; set; }

        public DateTime time { get; set; }

        public decimal Value { get; set; }
        public string summary { get; set; }

        public string type { get; set; }

        public int MixedCalculationId { get; set; }
        public MixedCalculation mixedCalculation { get; set; }
    }
}
