﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AMS.Entities
{
   public class SaleTemplateFile
    {
        [Key]
        public int Id { get; set; }

        public string SaleCode { get; set; }

        public string filePath { get; set; }

        public DateTime uploaddate { get; set; }
    }
}
