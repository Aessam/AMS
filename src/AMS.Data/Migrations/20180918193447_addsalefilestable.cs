﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AMS.Data.Migrations
{
    public partial class addsalefilestable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "SaleStatus",
                table: "Sales",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SaleType",
                table: "Sales",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "SaleFiles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    filename = table.Column<string>(nullable: true),
                    description = table.Column<string>(nullable: true),
                    SaleId = table.Column<int>(nullable: false),
                    uploaddate = table.Column<DateTime>(nullable: false),
                    FilePath = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SaleFiles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SaleFiles_Sales_SaleId",
                        column: x => x.SaleId,
                        principalTable: "Sales",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SaleFinalTemplate",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SaleId = table.Column<int>(nullable: false),
                    filePath = table.Column<string>(nullable: true),
                    uploaddate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SaleFinalTemplate", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SaleFiles_SaleId",
                table: "SaleFiles",
                column: "SaleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SaleFiles");

            migrationBuilder.DropTable(
                name: "SaleFinalTemplate");

            migrationBuilder.DropColumn(
                name: "SaleType",
                table: "Sales");

            migrationBuilder.AlterColumn<int>(
                name: "SaleStatus",
                table: "Sales",
                nullable: true,
                oldClrType: typeof(int));
        }
    }
}
