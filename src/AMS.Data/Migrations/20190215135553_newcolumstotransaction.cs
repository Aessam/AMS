﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AMS.Data.Migrations
{
    public partial class newcolumstotransaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MixedCalculationId",
                table: "MixedCalculation_Transactions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_MixedCalculation_Transactions_MixedCalculationId",
                table: "MixedCalculation_Transactions",
                column: "MixedCalculationId");

            migrationBuilder.AddForeignKey(
                name: "FK_MixedCalculation_Transactions_MixedCalculation_MixedCalculationId",
                table: "MixedCalculation_Transactions",
                column: "MixedCalculationId",
                principalTable: "MixedCalculation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MixedCalculation_Transactions_MixedCalculation_MixedCalculationId",
                table: "MixedCalculation_Transactions");

            migrationBuilder.DropIndex(
                name: "IX_MixedCalculation_Transactions_MixedCalculationId",
                table: "MixedCalculation_Transactions");

            migrationBuilder.DropColumn(
                name: "MixedCalculationId",
                table: "MixedCalculation_Transactions");
        }
    }
}
