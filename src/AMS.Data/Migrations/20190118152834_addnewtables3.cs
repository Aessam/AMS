﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AMS.Data.Migrations
{
    public partial class addnewtables3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FK_CustomerId",
                table: "Lot_Buyers");

            migrationBuilder.DropColumn(
                name: "FK_LotId",
                table: "Lot_Buyers");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "FK_CustomerId",
                table: "Lot_Buyers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "FK_LotId",
                table: "Lot_Buyers",
                nullable: false,
                defaultValue: 0);
        }
    }
}
