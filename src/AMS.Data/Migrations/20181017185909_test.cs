﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AMS.Data.Migrations
{
    public partial class test : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
          
            migrationBuilder.CreateTable(
                name: "Evaluations",
                columns: table => new
                {
                    CreatedBy = table.Column<long>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    LastUpdatedBy = table.Column<long>(nullable: true),
                    LastUpdatDate = table.Column<DateTime>(nullable: true),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    code = table.Column<string>(nullable: false),
                    ClientName = table.Column<string>(nullable: true),
                    EvaluationType = table.Column<int>(nullable: false),
                    PreviewDate = table.Column<DateTime>(nullable: true),
                    Fees = table.Column<decimal>(nullable: true),
                    CollectionStatus = table.Column<int>(nullable: true),
                    CollectionType = table.Column<int>(nullable: true),
                    ValueAddedTax = table.Column<decimal>(nullable: true),
                    SupplyStatus = table.Column<int>(nullable: true),
                    Supplydate = table.Column<DateTime>(nullable: true),
                    generalTaxStatus = table.Column<int>(nullable: true),
                    generaltax = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Evaluations", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Evaluations");

   
        }
    }
}
