﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AMS.Data.Migrations
{
    public partial class addnewtables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    CustomerId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Mobile = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.CustomerId);
                });

            migrationBuilder.CreateTable(
                name: "Lot",
                columns: table => new
                {
                    LotId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LotNumber = table.Column<string>(nullable: true),
                    FK_SaleID = table.Column<int>(nullable: false),
                    LotType = table.Column<string>(nullable: true),
                    BuyValue = table.Column<decimal>(nullable: false, defaultValue: 0m),
                    WeightorNumber = table.Column<int>(nullable: false, defaultValue: 0),
                    priceperweightornumber = table.Column<decimal>(nullable: false, defaultValue: 0m),
                    Paidamount = table.Column<decimal>(nullable: false, defaultValue: 0m),
                    InsurancePercentage = table.Column<decimal>(nullable: false, defaultValue: 0m),
                    FeesPercentage = table.Column<decimal>(nullable: false, defaultValue: 0m),
                    Total = table.Column<decimal>(nullable: false, defaultValue: 0m),
                    Reminder = table.Column<decimal>(nullable: false, defaultValue: 0m),
                    saleId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lot", x => x.LotId);
                    table.ForeignKey(
                        name: "FK_Lot_Sales_saleId",
                        column: x => x.saleId,
                        principalTable: "Sales",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Lot_Buyers",
                columns: table => new
                {
                    Lot_BuyersId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FK_LotId = table.Column<int>(nullable: false),
                    FK_CustomerId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lot_Buyers", x => x.Lot_BuyersId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Lot_saleId",
                table: "Lot",
                column: "saleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "Lot");

            migrationBuilder.DropTable(
                name: "Lot_Buyers");
        }
    }
}
