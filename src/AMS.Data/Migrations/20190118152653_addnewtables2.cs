﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AMS.Data.Migrations
{
    public partial class addnewtables2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LotId",
                table: "Lot_Buyers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Lot_BuyersId1",
                table: "Lot_Buyers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Lot_Buyers_LotId",
                table: "Lot_Buyers",
                column: "LotId");

            migrationBuilder.CreateIndex(
                name: "IX_Lot_Buyers_Lot_BuyersId1",
                table: "Lot_Buyers",
                column: "Lot_BuyersId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Lot_Buyers_Lot_LotId",
                table: "Lot_Buyers",
                column: "LotId",
                principalTable: "Lot",
                principalColumn: "LotId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Lot_Buyers_Lot_Buyers_Lot_BuyersId1",
                table: "Lot_Buyers",
                column: "Lot_BuyersId1",
                principalTable: "Lot_Buyers",
                principalColumn: "Lot_BuyersId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Lot_Buyers_Lot_LotId",
                table: "Lot_Buyers");

            migrationBuilder.DropForeignKey(
                name: "FK_Lot_Buyers_Lot_Buyers_Lot_BuyersId1",
                table: "Lot_Buyers");

            migrationBuilder.DropIndex(
                name: "IX_Lot_Buyers_LotId",
                table: "Lot_Buyers");

            migrationBuilder.DropIndex(
                name: "IX_Lot_Buyers_Lot_BuyersId1",
                table: "Lot_Buyers");

            migrationBuilder.DropColumn(
                name: "LotId",
                table: "Lot_Buyers");

            migrationBuilder.DropColumn(
                name: "Lot_BuyersId1",
                table: "Lot_Buyers");
        }
    }
}
