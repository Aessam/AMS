﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AMS.Data.Migrations
{
    public partial class newtables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
           
         

      

            migrationBuilder.CreateTable(
                name: "MixedCalculation",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    SaleId = table.Column<int>(nullable: false),
                    TotalValue = table.Column<decimal>(nullable: false),
                    Isclosed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MixedCalculation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MixedCalculation_Sales_SaleId",
                        column: x => x.SaleId,
                        principalTable: "Sales",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MixedCalculation_Transactions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    time = table.Column<DateTime>(nullable: false),
                    Value = table.Column<decimal>(nullable: false),
                    summary = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MixedCalculation_Transactions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MixedCalculation_Lots",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MixedCalculationId = table.Column<int>(nullable: false),
                    LotId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MixedCalculation_Lots", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MixedCalculation_Lots_Lot_LotId",
                        column: x => x.LotId,
                        principalTable: "Lot",
                        principalColumn: "LotId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MixedCalculation_Lots_MixedCalculation_MixedCalculationId",
                        column: x => x.MixedCalculationId,
                        principalTable: "MixedCalculation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MixedCalculation_SaleId",
                table: "MixedCalculation",
                column: "SaleId");

            migrationBuilder.CreateIndex(
                name: "IX_MixedCalculation_Lots_LotId",
                table: "MixedCalculation_Lots",
                column: "LotId");

            migrationBuilder.CreateIndex(
                name: "IX_MixedCalculation_Lots_MixedCalculationId",
                table: "MixedCalculation_Lots",
                column: "MixedCalculationId");

        
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
           

            migrationBuilder.DropTable(
                name: "MixedCalculation_Lots");

            migrationBuilder.DropTable(
                name: "MixedCalculation_Transactions");

            migrationBuilder.DropTable(
                name: "MixedCalculation");

         
        }
    }
}
