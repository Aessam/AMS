﻿// <auto-generated />
using System;
using AMS.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace AMS.Data.Migrations
{
    [DbContext(typeof(AmsDbContext))]
    [Migration("20180622171638_createDB")]
    partial class createDB
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.1-rtm-30846")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("AMS.Entities.Sale", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Code");

                    b.Property<int>("CollectionStatus");

                    b.Property<int>("CollectionType");

                    b.Property<decimal>("Commission");

                    b.Property<decimal>("CommissionRate");

                    b.Property<long>("CreatedBy");

                    b.Property<DateTime>("CreationDate");

                    b.Property<DateTime>("Date");

                    b.Property<string>("Description");

                    b.Property<decimal>("Fees");

                    b.Property<string>("InstitutionName");

                    b.Property<DateTime?>("LastUpdatDate")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(null);

                    b.Property<long?>("LastUpdatedBy")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(null);

                    b.Property<DateTime>("SupplyDate");

                    b.Property<int>("SupplyStatus");

                    b.Property<decimal>("TotalSales");

                    b.Property<decimal>("ValueAddedTax");

                    b.HasKey("Id");

                    b.ToTable("Sales");
                });
#pragma warning restore 612, 618
        }
    }
}
