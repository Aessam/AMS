﻿using AMS.Data.Repos.Interfaces;
using AMS.Entities.Evaluations;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMS.Data.Repos
{
    public class EvaluationTemplateFileRepository : BaseRepository<EvaluationTemplateFile>, IEvaluationTemplateFileRepository
    {
        public EvaluationTemplateFileRepository(AmsDbContext context) : base(context)
        {
        }
    }
}
