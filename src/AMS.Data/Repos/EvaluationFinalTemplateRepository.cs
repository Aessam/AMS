﻿using AMS.Data.Repos.Interfaces;
using AMS.Entities.Evaluations;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMS.Data.Repos
{
   public class EvaluationFinalTemplateRepository:BaseRepository<EvaluationFinalTemplate>,IEvaluationFinalTemplateRepository
    {
        public EvaluationFinalTemplateRepository(AmsDbContext context) : base(context)
        {

        }
    }
}
