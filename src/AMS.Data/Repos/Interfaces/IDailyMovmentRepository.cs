﻿using AMS.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMS.Data.Repos.Interfaces
{
    public interface IDailyMovmentRepository : IBaseRepository<DailyMovments>
    {
    }
}
