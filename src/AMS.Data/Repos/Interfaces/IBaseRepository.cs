﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AMS.Data.Repos.Interfaces
{
    public interface IBaseRepository<TEntity> : IDisposable where TEntity : class
    {

        /// <summary>
        /// Gets an entity by its id
        /// </summary>
        /// <param name="id">Entity's Identifier</param>
        /// <returns>The entity</returns>
        TEntity Get(long id);

        /// <summary>
        /// Get all entity
        /// </summary>
        /// <returns></returns>
        IEnumerable<TEntity> GetAll();
        /// <summary>
        /// Adds an entity to the entity set
        /// </summary>
        /// <param name="entity">Entity to be added</param>
        void Add(TEntity entity);

        Task AddAsync(TEntity entity);

        void Update(TEntity entity);


        void Delete(TEntity entity);

        void Delete(long entityId);

        int Save();

        Task<int> SaveAsync();



        List<TEntity> Filter(params Expression<Func<TEntity, bool>>[] predicates);
    }
}
