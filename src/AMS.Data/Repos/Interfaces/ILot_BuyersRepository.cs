﻿using AMS.Entities.Calculations;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMS.Data.Repos.Interfaces
{
    public interface ILot_BuyersRepository : IBaseRepository<Lot_Buyers>
    {

    }
}
