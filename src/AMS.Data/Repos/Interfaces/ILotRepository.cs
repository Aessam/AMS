﻿using AMS.Entities.Calculations;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AMS.Data.Repos.Interfaces
{
    public interface ILotRepository : IBaseRepository<Lot>
    {
        Task<object> getall();
        Task<object> getsalelotdetails(int saleid);
        Task<object> getsalelotdetails(List<int> lotIds);
    }
}
