﻿using AMS.Entities.Calculations;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AMS.Data.Repos.Interfaces
{
    public interface IMixedCalculationRepository : IBaseRepository<MixedCalculation>
    {
        Task<List<int>> GetUsedLots(int saleID);
    }
}
