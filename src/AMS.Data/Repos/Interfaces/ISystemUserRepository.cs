﻿using AMS.Entities;

namespace AMS.Data.Repos.Interfaces
{
    public interface ISystemUserRepository : IBaseRepository<SystemUser>
    {

    }
}
