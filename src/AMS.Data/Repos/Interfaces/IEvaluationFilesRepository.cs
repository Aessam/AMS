﻿using AMS.Entities.Evaluations;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMS.Data.Repos.Interfaces
{
   public interface IEvaluationFilesRepository:IBaseRepository<EvaluationFile>
    {
    }
}
