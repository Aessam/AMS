﻿using System;
using System.Collections.Generic;
using System.Text;
using AMS.Entities;

namespace AMS.Data.Repos.Interfaces
{
    public interface IModuleRepository:IBaseRepository<Module>
    {
    }
}
