﻿using AMS.Entities.Customers;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMS.Data.Repos.Interfaces
{
    public interface ICustomerRepository:IBaseRepository<Customer>
    {

    }
}
