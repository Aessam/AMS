﻿using AMS.Entities.Calculations;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMS.Data.Repos.Interfaces
{
    public interface IMixedCalculation_LotsRepository : IBaseRepository<MixedCalculation_Lots>
    {
    }
}
