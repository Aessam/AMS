﻿using AMS.Data.Repos.Interfaces;
using AMS.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMS.Data.Repos
{
    public class SaleTemplateFileRepository : BaseRepository<SaleTemplateFile>, ISaleTemplateFileRepository
    {
        public SaleTemplateFileRepository(AmsDbContext context) : base(context)
        {

        }
    }
}
