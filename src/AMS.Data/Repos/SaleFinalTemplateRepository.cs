﻿using AMS.Data.Repos.Interfaces;
using AMS.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMS.Data.Repos
{
    public class SaleFinalTemplateRepository:BaseRepository<SaleFinalTemplate> ,ISaleFinalTemplateRepository
    {
        public SaleFinalTemplateRepository(AmsDbContext context) : base(context)
        {

        }
    }
}
