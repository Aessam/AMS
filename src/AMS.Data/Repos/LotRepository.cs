﻿using AMS.Data.Repos.Interfaces;
using AMS.Entities.Calculations;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AMS.Data.Repos
{
   public class LotRepository:BaseRepository<Lot>,ILotRepository
    {
        public LotRepository(AmsDbContext context) : base(context)
        {

        }

        public async Task<object> getall()
        {
            var x = await _context.Lot.Include(s => s.sale).Select(l => new 
            {
                Id = l.sale.Id,
                Code = l.sale.Code,
                Description = l.sale.Description,
                SaleName = l.sale.InstitutionName,
                date = l.sale.Date
            }).Distinct().OrderByDescending(l=>l.date).ToListAsync();

            return x;
        }

        public async Task<object> getsalelotdetails(int saleid)
        {
            var data =  _context.Lot.Include(l => l.Buyers).Where(l => l.FK_SaleID == saleid).Select(l=>new
            {
                l.LotId,
                l.LotNumber,
                l.LotType,
                l.InsurancePercentage,
                l.BuyValue,
                l.Paidamount,
                l.Reminder,
                l.FeesPercentage,
                l.Total,
                buyers=l.Buyers.Select(b=>new {b.CustomerId }).ToList()
            }).ToList();

         


            return data;

        }
        public async Task<object> getsalelotdetails(List<int> lotIds)
        {
            var data = _context.Lot.Include(l => l.Buyers).Where(l => lotIds.Contains(l.LotId)).Select(l => new
            {
                l.LotId,
                l.LotNumber,
                l.LotType,
                l.InsurancePercentage,
                l.BuyValue,
                l.Paidamount,
                l.Reminder,
                l.FeesPercentage,
                l.Total,
                buyers = l.Buyers.Select(b => new { b.CustomerId }).ToList()
            }).ToList();




            return data;

        }


    }
}
