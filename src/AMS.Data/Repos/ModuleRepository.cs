﻿using System;
using System.Collections.Generic;
using System.Text;
using AMS.Data.Repos.Interfaces;
using AMS.Entities;

namespace AMS.Data.Repos
{
    public class ModuleRepository:BaseRepository<Module>,IModuleRepository
    {
        public ModuleRepository(AmsDbContext context) : base(context)
        {
        }
    }
}
