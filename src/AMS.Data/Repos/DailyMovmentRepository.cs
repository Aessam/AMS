﻿using AMS.Data.Repos.Interfaces;
using AMS.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMS.Data.Repos
{
    public class DailyMovmentRepository:BaseRepository<DailyMovments>,IDailyMovmentRepository
    {
        public DailyMovmentRepository(AmsDbContext context) : base(context)
        {

        }
    }
}
