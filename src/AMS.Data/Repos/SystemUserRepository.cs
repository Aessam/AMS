﻿using AMS.Entities;
using AMS.Data.Repos.Interfaces;
namespace AMS.Data.Repos
{
    public class SystemUserRepository:BaseRepository<SystemUser>,ISystemUserRepository
    {
        public SystemUserRepository(AmsDbContext context) :base(context)
        {

        }
    }
}
