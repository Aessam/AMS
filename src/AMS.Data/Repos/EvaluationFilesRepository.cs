﻿using AMS.Data.Repos.Interfaces;
using AMS.Entities.Evaluations;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMS.Data.Repos
{
   public class EvaluationFilesRepository:BaseRepository<EvaluationFile>,IEvaluationFilesRepository
    {
        public EvaluationFilesRepository(AmsDbContext context) : base(context)
        {
        }
    }
}
