﻿using AMS.Data.Repos.Interfaces;
using AMS.Entities.Calculations;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.Data.Repos
{
   public class MixedCalculationRepository:BaseRepository<MixedCalculation>,IMixedCalculationRepository
    {
        public MixedCalculationRepository(AmsDbContext db) : base(db)
        {

        }

        public async Task<List<int>> GetUsedLots(int saleID)
        {
            var data = await _context.MixedCalculation_Lots.Include(m => m.MixedCalculation).Where(m => m.MixedCalculation.SaleId == saleID).
                Select(m => m.LotId).ToListAsync();

            return data;

        }
     

    }
}
