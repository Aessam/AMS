﻿using System;
using System.Collections.Generic;
using System.Text;
using AMS.Data.Repos.Interfaces;
using AMS.Entities;

namespace AMS.Data.Repos
{
    public class SaleRepository:BaseRepository<Sale>,ISaleRepository
    {
        public SaleRepository(AmsDbContext context) : base(context)
        {

        }
    }
}
