﻿using AMS.Data.Repos.Interfaces;
using AMS.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMS.Data.Repos
{
   public class SaleFileRepository:BaseRepository<SaleFiles>,ISaleFileRepository
    {
        public SaleFileRepository(AmsDbContext context) : base(context)
        {

        }
    }
}
