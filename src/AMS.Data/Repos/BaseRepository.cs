﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AMS.Data.Repos
{
    public class BaseRepository<T> : IDisposable where T : class
    {
        protected AmsDbContext _context;

        public BaseRepository(AmsDbContext context)
        {
            _context = context;
        }


        public void Add(T entity)
        {
            _context.Set<T>().Add(entity);
        }

        public Task AddAsync(T entity)
        {
            return _context.Set<T>().AddAsync(entity);
        }

        public virtual void Delete(T entity)
        {
            _context.Set<T>().Remove(entity);
        }

        public virtual void Delete(long id)
        {
            var entity = Get(id);
            if (entity == null) return; // not found; assume already deleted.
            Delete(entity);
        }

        public int Save()
        {
            return _context.SaveChanges();
        }

        public Task<int> SaveAsync()
        {
            return _context.SaveChangesAsync();
        }
        public T Get(long id)
        {
            return _context.Set<T>().Find(id);
        }
        public IEnumerable<T> GetAll()
        {
            return _context.Set<T>();
        }



        public void Update(T entity)
        {
            _context.Update(entity);
            _context.SaveChanges();
        }

        public List<T> Filter(params Expression<Func<T, bool>>[] predicates)
        {
            var q = _context.Set<T>().AsQueryable();

            foreach (var predicate in predicates)
            {
                q = q.Where(predicate);
            }

            return q.ToList();
        }


        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public virtual void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// The dispose method.
        /// </summary>
        /// <param name="disposing">If true, all resources allocated by this class will be disposed.</param>
        public virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                //Context.Dispose();
            }
        }
    }
}
