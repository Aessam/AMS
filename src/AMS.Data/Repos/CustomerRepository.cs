﻿using AMS.Data.Repos.Interfaces;
using AMS.Entities.Customers;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMS.Data.Repos
{
    public class CustomerRepository : BaseRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(AmsDbContext db) : base(db)
        {

        }
    }
}
