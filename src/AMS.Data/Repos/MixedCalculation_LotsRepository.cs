﻿using AMS.Data.Repos.Interfaces;
using AMS.Entities.Calculations;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMS.Data.Repos
{
   public class MixedCalculation_LotsRepository:BaseRepository<MixedCalculation_Lots>, IMixedCalculation_LotsRepository
    {
        public MixedCalculation_LotsRepository(AmsDbContext db) : base(db)
        {

        }
    }
}
