﻿using AMS.Data.Repos.Interfaces;
using AMS.Entities.Evaluations;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMS.Data.Repos
{
  public  class EvaluationRepository:BaseRepository<Evaluation>,IEvaluationRepository
    {
        public EvaluationRepository(AmsDbContext dbContext) : base(dbContext)
        {

        }
    }
}
