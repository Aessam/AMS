﻿using AMS.Data.Repos.Interfaces;
using AMS.Entities.Calculations;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMS.Data.Repos
{
    public class MixedCalculation_transactionsRepository : BaseRepository<MixedCalculation_Transactions>, IMixedCalculation_transactionsRepository
    {
        public MixedCalculation_transactionsRepository(AmsDbContext db) : base(db)
        { }
    }
}
