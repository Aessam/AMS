﻿using AMS.Data.Repos.Interfaces;
using AMS.Entities.Calculations;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMS.Data.Repos
{
   public class Lot_BuyersRepository:BaseRepository<Lot_Buyers>,ILot_BuyersRepository
    {
        public Lot_BuyersRepository(AmsDbContext db) : base(db)
        { }
    }
}
