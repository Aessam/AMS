﻿using System;
using System.IO;
using AMS.Data.EntityTypesMapping;
using AMS.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using AMS.Entities.Evaluations;
using AMS.Entities.Customers;
using AMS.Entities.Calculations;

namespace AMS.Data
{
    public class AmsDbContext:DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // get the configuration from the app settings
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
            optionsBuilder.UseSqlServer(config.GetConnectionString("DefaultConnection"));
            //optionsBuilder.UseSqlServer("Data Source=ec2-34-226-39-214.compute-1.amazonaws.com;Initial Catalog=CoursesBeta;user ID=portals.runner;Password=srjyhwho6shmhtetxhkzrrdglhgKZTGXTUHNSIW,LQWAFiXHZSIFTRJ6ThashesjkdjmgykaxHNAZFJdvsahKGJn;");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new SaleConfiguration());
            modelBuilder.ApplyConfiguration(new EvaluationConfiguration());
            modelBuilder.ApplyConfiguration(new LotConfiguration());
        }

        public DbSet<Sale> Sales { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<SystemUser> SystemUsers { get; set; }

        public DbSet<SaleFiles> SaleFiles { get; set; }

        public DbSet<SaleFinalTemplate> SaleFinalTemplate { get; set; }

        public DbSet<Evaluation> Evaluations { get; set; }

        public DbSet<EvaluationFile> EvaluationFiles { get; set; }
        public DbSet<EvaluationFinalTemplate> EvaluationFinalTemplates { get; set; }

        public DbSet<SaleTemplateFile> SaleTemplateFile { get; set; }
        public DbSet<EvaluationTemplateFile> EvaluationTemplateFile { get; set; }

        public DbSet<DailyMovments> DailyMovments { get; set; }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Lot> Lot { get; set; }
        public DbSet<Lot_Buyers> Lot_Buyers { get; set; }

        public DbSet<MixedCalculation> MixedCalculation { get; set; }

        public DbSet<MixedCalculation_Lots> MixedCalculation_Lots { get; set; }
        public DbSet<MixedCalculation_Transactions> MixedCalculation_Transactions { get; set; }
    }
}
