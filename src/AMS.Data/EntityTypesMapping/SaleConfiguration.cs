﻿using System;
using System.Collections.Generic;
using System.Text;
using AMS.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AMS.Data.EntityTypesMapping
{
    public class SaleConfiguration:IEntityTypeConfiguration<Sale>
    {
        public void Configure(EntityTypeBuilder<Sale> modelBuilder)
        {
            modelBuilder.Property(x => x.LastUpdatedBy).HasDefaultValue(null);
            modelBuilder.Property(x => x.LastUpdatDate).HasDefaultValue(null);
     
            modelBuilder.Property(x => x.CreatedBy).IsRequired(true);
            modelBuilder.Property(x => x.CreationDate).IsRequired(true);
        }
    }
}
