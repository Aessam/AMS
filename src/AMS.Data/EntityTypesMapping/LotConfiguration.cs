﻿using AMS.Entities.Calculations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMS.Data.EntityTypesMapping
{
    public class LotConfiguration : IEntityTypeConfiguration<Lot>
    {
        public void Configure(EntityTypeBuilder<Lot> modelBuilder)
        {
            modelBuilder.Property(x => x.BuyValue).HasDefaultValue(0);
            modelBuilder.Property(x => x.FeesPercentage).HasDefaultValue(0);
            modelBuilder.Property(x => x.InsurancePercentage).HasDefaultValue(0);
            modelBuilder.Property(x => x.Paidamount).HasDefaultValue(0);
            modelBuilder.Property(x => x.priceperweightornumber).HasDefaultValue(0);
            modelBuilder.Property(x => x.WeightorNumber).HasDefaultValue(0);
            modelBuilder.Property(x => x.Total).HasDefaultValue(0);
            modelBuilder.Property(x => x.Reminder).HasDefaultValue(0);

        }
    }
}
