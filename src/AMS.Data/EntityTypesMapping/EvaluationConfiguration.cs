﻿using AMS.Entities.Evaluations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMS.Data.EntityTypesMapping
{
    public class EvaluationConfiguration:IEntityTypeConfiguration<Evaluation>
    {
        public void Configure(EntityTypeBuilder<Evaluation> modelBuilder)
        {
            modelBuilder.Property(x => x.LastUpdatedBy).HasDefaultValue(null);
            modelBuilder.Property(x => x.LastUpdatDate).HasDefaultValue(null);

            modelBuilder.Property(x => x.CreatedBy).IsRequired(true);
            modelBuilder.Property(x => x.CreationDate).IsRequired(true);
        }
    }
}
